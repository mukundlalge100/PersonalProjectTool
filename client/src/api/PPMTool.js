import axios from "axios";

const PPMTool = axios.create({
  baseURL: "http://localhost:8090/ppmtool",
});

export default PPMTool;
