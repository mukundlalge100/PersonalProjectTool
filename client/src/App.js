import React, { Component, Suspense } from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import Loader from "./Components/UI/Loader/Loader";
import classes from "./App.module.scss";
import utilClasses from "./Util/Util.module.scss";
import Navbar from "./Containers/Layouts/Navbar/Navbar";
import Footer from "./Components/Layouts/Footer/Footer";
import { authCheckLogInState } from "./store/Actions/AuthAction";
import Routing from "./Components/Routing/Routing";

class App extends Component {
  componentDidMount = () => {
    this.props.onAuthCheckLogInState(this.props.history);
  };
  render() {
    const { isAuthenticated } = this.props;
    return (
      <main className={classes.App}>
        <Suspense
          fallback={
            <div className={utilClasses.Loader__Centered}>
              <Loader />
            </div>
          }
        >
          <Navbar />
          <Routing isAuthenticated={isAuthenticated} />
          <Footer />
        </Suspense>
      </main>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    isAuthenticated: state.authReducer.isAuthenticated,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    onAuthCheckLogInState: (history) => dispatch(authCheckLogInState(history)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(App));
