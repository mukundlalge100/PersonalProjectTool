import axios from "../../api/PPMTool";
import { actionTypes } from "./ActionTypes";

export const clearErrors = () => {
  return {
    type: actionTypes.CLEAR_PROJECT_ERRORS
  };
};
const createProjectStart = () => {
  return {
    type: actionTypes.ADD_PROJECT_START
  };
};
const createProjectSuccess = project => {
  return {
    type: actionTypes.ADD_PROJECT_SUCCESS,
    project
  };
};
const createProjectFail = errors => {
  return {
    type: actionTypes.ADD_PROJECT_FAIL,
    errors
  };
};

export const createProject = (project, history) => async dispatch => {
  try {
    dispatch(createProjectStart());
    const response = await axios.post("/api/project", project);
    dispatch(createProjectSuccess(response.data));
    history.push("/");
  } catch (error) {
    dispatch(createProjectFail(error.response.data));
  }
};

const updateProjectStart = () => {
  return {
    type: actionTypes.UPDATE_PROJECT_START
  };
};
const updateProjectSuccess = project => {
  return {
    type: actionTypes.UPDATE_PROJECT_SUCCESS,
    project
  };
};
const updateProjectFail = errors => {
  return {
    type: actionTypes.UPDATE_PROJECT_FAIL,
    errors
  };
};

export const updateProject = (
  project,
  projectIdentifier,
  history
) => async dispatch => {
  try {
    dispatch(updateProjectStart());
    const response = await axios.put(
      `/api/project/${projectIdentifier}`,
      project
    );
    dispatch(updateProjectSuccess(response.data));
    history.push("/");
  } catch (error) {
    dispatch(updateProjectFail(error.response.data));
  }
};

const getProjectsStart = () => {
  return {
    type: actionTypes.GET_PROJECTS_START
  };
};
const getProjectsSuccess = data => {
  return {
    type: actionTypes.GET_PROJECTS_SUCCESS,
    data
  };
};
const getProjectsFail = errors => {
  return {
    type: actionTypes.GET_PROJECTS_FAIL,
    errors
  };
};

export const getProjects = pageNumber => async dispatch => {
  try {
    dispatch(getProjectsStart());
    const response = await axios.get(`/api/project/all/${pageNumber}`);
    dispatch(getProjectsSuccess(response.data));
  } catch (error) {
    dispatch(getProjectsFail(error));
  }
};

const getProjectStart = () => {
  return {
    type: actionTypes.GET_PROJECT_START
  };
};
const getProjectSuccess = project => {
  return {
    type: actionTypes.GET_PROJECT_SUCCESS,
    project
  };
};
const getProjectFail = errors => {
  return {
    type: actionTypes.GET_PROJECT_FAIL,
    errors
  };
};

export const getProject = projectId => async dispatch => {
  try {
    dispatch(getProjectStart());
    const response = await axios.get(`/api/project/${projectId}`);
    dispatch(getProjectSuccess(response.data));
  } catch (error) {
    dispatch(getProjectFail(error));
  }
};

const deleteProjectStart = () => {
  return {
    type: actionTypes.DELETE_PROJECT_START
  };
};
const deleteProjectSuccess = projectIdentifier => {
  return {
    type: actionTypes.DELETE_PROJECT_SUCCESS,
    projectIdentifier
  };
};
const deleteProjectFail = errors => {
  return {
    type: actionTypes.DELETE_PROJECT_FAIL,
    errors
  };
};

export const deleteProject = (
  projectIdentifier,
  currentPage
) => async dispatch => {
  try {
    dispatch(deleteProjectStart());
    const response = await axios.delete(`/api/project/${projectIdentifier}`);
    if (response.data.success) {
      dispatch(deleteProjectSuccess(projectIdentifier));
      dispatch(getProjects(currentPage));
    }
  } catch (error) {
    dispatch(deleteProjectFail(error));
  }
};
