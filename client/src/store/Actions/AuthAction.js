import { actionTypes } from "./ActionTypes";
import axios from "../../api/PPMTool";
import jwtDecode from "jwt-decode";
import { setAuthToken } from "../../Util/Util";

export const clearAuthSignUpErrors = () => {
  return {
    type: actionTypes.CLEAR_AUTH_SIGNUP_ERRORS
  };
};
export const clearAuthLogInErrors = () => {
  return {
    type: actionTypes.CLEAR_AUTH_LOGIN_ERRORS
  };
};

const authSignUpStart = () => {
  return {
    type: actionTypes.AUTH_SIGNUP_START
  };
};
const authSignUpSuccess = () => {
  return {
    type: actionTypes.AUTH_SIGNUP_SUCCESS
  };
};
const authSignUpFail = errors => {
  return {
    type: actionTypes.AUTH_SIGNUP_FAIL,
    errors: errors
  };
};
export const authSignUp = (userData, history) => {
  return async dispatch => {
    dispatch(authSignUpStart());
    try {
      const response = await axios.post("/api/user/signup", userData);

      if (response.data.success) {
        dispatch(authSignUpSuccess());
      }
      history.push("/login");
    } catch (error) {
      dispatch(authSignUpFail(error.response.data));
    }
  };
};
const authLogInStart = () => {
  return {
    type: actionTypes.AUTH_LOGIN_START
  };
};
const authLogInSuccess = user => {
  return {
    type: actionTypes.AUTH_LOGIN_SUCCESS,
    user
  };
};

const authLogInFail = errors => {
  return {
    type: actionTypes.AUTH_LOGIN_FAIL,
    errors
  };
};

export const authLogIn = (userData, history) => {
  return async dispatch => {
    dispatch(authLogInStart());
    try {
      const response = await axios.post("/api/user/login", userData);
      const { token } = response.data;
      localStorage.setItem("jwtToken", token);
      setAuthToken(token);
      const user = jwtDecode(token);
      dispatch(authLogInSuccess(user));
      history.push("/dashboard");
    } catch (error) {
      dispatch(authLogInFail(error.response.data));
    }
  };
};

// LOGOUT ACTIONS ...
export const authLogOutStart = () => {
  return {
    type: actionTypes.AUTH_LOGOUT_START
  };
};
export const authLogOutSuccess = () => {
  return {
    type: actionTypes.AUTH_LOGOUT_SUCCESS
  };
};
export const authLogOutFail = errors => {
  return {
    type: actionTypes.AUTH_LOGOUT_FAIL,
    errors
  };
};
export const authLogOut = history => {
  return async dispatch => {
    try {
      dispatch(authLogOutStart());
      const response = await axios.get("/api/user/logout");
      if (response.data.success) {
        dispatch(authLogOutSuccess());
        history.push("/");
      }
    } catch (error) {
      dispatch(authLogOutFail(error));
    }
  };
};

export const checkExpiredTime = (expiredTime, history) => {
  return dispatch => {
    setTimeout(() => {
      dispatch(authLogOut(history));
    }, expiredTime * 1000);
  };
};

export const authCheckLogInState = history => {
  return async dispatch => {
    if (localStorage.jwtToken) {
      setAuthToken(localStorage.jwtToken);
      const user = jwtDecode(localStorage.jwtToken);
      const currenTime = Date.now() / 1000;
      if (user.exp < currenTime) {
        dispatch(authLogOut(history));
        return;
      }
      const totalExpTimeLeft = user.exp - currenTime;
      dispatch(authLogInSuccess(user));
      dispatch(checkExpiredTime(totalExpTimeLeft, history));
    }
  };
};

// RESET PASSWORD ACTIONS ...
export const authResetPasswordStart = () => {
  return {
    type: actionTypes.AUTH_RESET_PASSWORD_START
  };
};
export const authResetPasswordSuccess = () => {
  return {
    type: actionTypes.AUTH_RESET_PASSWORD_SUCCESS
  };
};
export const authResetPasswordFail = errors => {
  return {
    type: actionTypes.AUTH_RESET_PASSWORD_FAIL,
    errors
  };
};

export const authResetPassword = (userData, history) => {
  return async dispatch => {
    dispatch(authResetPasswordStart());
    try {
      const response = await axios.post("/users/reset-password", userData);
      if (response.data.success) {
        dispatch(authResetPasswordSuccess());
        alert(response.data.message);
        history.push("/login");
      }
    } catch (error) {
      dispatch(authResetPasswordFail(error.response.data));
    }
  };
};

// NEW PASSWORD ACTIONS ...
export const authNewPasswordStart = () => {
  return {
    type: actionTypes.AUTH_NEW_PASSWORD_START
  };
};
export const authNewPasswordSuccess = () => {
  return {
    type: actionTypes.AUTH_NEW_PASSWORD_SUCCESS
  };
};
export const authNewPasswordFail = errors => {
  return {
    type: actionTypes.AUTH_NEW_PASSWORD_FAIL,
    errors
  };
};

export const authNewPassword = (userData, token, history) => {
  return async dispatch => {
    dispatch(authNewPasswordStart());
    try {
      const response = await axios.post("/users/new-password", {
        userData,
        token
      });
      if (response.data.success) {
        dispatch(authNewPasswordSuccess());
        alert(response.data.message);
        history.push("/login");
      }
    } catch (error) {
      dispatch(authNewPasswordFail(error.response.data));
    }
  };
};
