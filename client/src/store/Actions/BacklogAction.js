import axios from "../../api/PPMTool";
import { actionTypes } from "./ActionTypes";

const getProjectTasksStart = () => {
  return {
    type: actionTypes.GET_PROJECT_TASKS_START
  };
};
const getProjectTasksSuccess = data => {
  return {
    type: actionTypes.GET_PROJECT_TASKS_SUCCESS,
    data
  };
};
const getProjectTasksFail = errors => {
  return {
    type: actionTypes.GET_PROJECT_TASKS_FAIL,
    errors
  };
};

export const getProjectTasks = projectIdentifier => async dispatch => {
  try {
    dispatch(getProjectTasksStart());
    const response = await axios.get(`/api/backlog/${projectIdentifier}`);
    dispatch(getProjectTasksSuccess(response.data));
  } catch (error) {
    dispatch(getProjectTasksFail(error.response.data));
  }
};

const getProjectTaskStart = () => {
  return {
    type: actionTypes.GET_PROJECT_TASK_START
  };
};
const getProjectTaskSuccess = data => {
  return {
    type: actionTypes.GET_PROJECT_TASK_SUCCESS,
    data
  };
};
const getProjectTaskFail = errors => {
  return {
    type: actionTypes.GET_PROJECT_TASK_FAIL,
    errors
  };
};

export const getProjectTask = (
  projectIdentifier,
  projectSequence
) => async dispatch => {
  try {
    dispatch(getProjectTaskStart());
    const response = await axios.get(
      `/api/backlog/${projectIdentifier}/${projectSequence}`
    );
    dispatch(getProjectTaskSuccess(response.data));
  } catch (error) {
    dispatch(getProjectTaskFail(error.response.data));
  }
};

const createProjectTaskStart = () => {
  return {
    type: actionTypes.CREATE_PROJECT_TASK_START
  };
};
const createProjectTaskSuccess = data => {
  return {
    type: actionTypes.CREATE_PROJECT_TASK_SUCCESS,
    data
  };
};
const createProjectTaskFail = errors => {
  return {
    type: actionTypes.CREATE_PROJECT_TASK_FAIL,
    errors
  };
};
export const createProjectTask = (
  task,
  projectIdentifier,
  history
) => async dispatch => {
  try {
    dispatch(createProjectTaskStart());
    const response = await axios.post(
      `/api/backlog/${projectIdentifier}`,
      task
    );
    dispatch(createProjectTaskSuccess(response.data));
    history.push(`/projectBoard/${projectIdentifier}`);
  } catch (error) {
    dispatch(createProjectTaskFail(error.response.data));
  }
};

const updateProjectTaskStart = () => {
  return {
    type: actionTypes.UPDATE_PROJECT_TASK_START
  };
};
const updateProjectTaskSuccess = data => {
  return {
    type: actionTypes.UPDATE_PROJECT_TASK_SUCCESS,
    data
  };
};
const updateProjectTaskFail = errors => {
  return {
    type: actionTypes.UPDATE_PROJECT_TASK_FAIL,
    errors
  };
};
export const updateProjectTask = (
  task,
  projectIdentifier,
  projectSequence,
  history
) => async dispatch => {
  try {
    dispatch(updateProjectTaskStart());
    const response = await axios.patch(
      `/api/backlog/${projectIdentifier}/${projectSequence}`,
      task
    );
    dispatch(updateProjectTaskSuccess(response.data));
    history.push(`/projectBoard/${projectIdentifier}`);
  } catch (error) {
    dispatch(updateProjectTaskFail(error.response.data));
  }
};

const deleteProjectTaskSuccess = projectSequence => {
  return {
    type: actionTypes.DELETE_PROJECT_TASK,
    projectSequence
  };
};
export const deleteProjectTask = (
  projectIdentifier,
  projectSequence
) => async dispatch => {
  try {
    const response = await axios.delete(
      `/api/backlog/${projectIdentifier}/${projectSequence}`
    );
    if (response.data.success) {
      dispatch(deleteProjectTaskSuccess(projectSequence));
    }
  } catch (error) {
    console.log(error);
  }
};

export const clearErrors = () => {
  return {
    type: actionTypes.CLEAR_BACKLOG_ERRORS
  };
};
