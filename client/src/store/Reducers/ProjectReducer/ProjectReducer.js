import { updateObject } from "../../../Util/Util";
import { actionTypes } from "../../Actions/ActionTypes";

const initialState = {
  project: null,
  projects: [],
  projectLoading: false,
  errors: {},
  itemsCountPerPage: null,
  totalItemsCount: null
};

const createProjectStart = state => {
  return updateObject(state, {
    projectLoading: true
  });
};
const createProjectSuccess = (state, action) => {
  return updateObject(state, {
    projectLoading: false,
    project: action.project
  });
};
const createProjectFail = (state, action) => {
  return updateObject(state, {
    projectLoading: false,
    errors: action.errors
  });
};

const updateProjectStart = state => {
  return updateObject(state, {
    projectLoading: true
  });
};
const updateProjectSuccess = (state, action) => {
  return updateObject(state, {
    projectLoading: false,
    project: action.project
  });
};
const updateProjectFail = (state, action) => {
  return updateObject(state, {
    projectLoading: false,
    errors: action.errors
  });
};

const getProjectsStart = state => {
  return updateObject(state, {
    projectLoading: true
  });
};
const getProjectsSuccess = (state, action) => {
  return updateObject(state, {
    projectLoading: false,
    projects: action.data.content,
    totalPages: action.data.totalPages,
    totalItemsCount: action.data.totalElements,
    itemsCountPerPage: action.data.size
  });
};
const getProjectsFail = (state, action) => {
  return updateObject(state, {
    projectLoading: false,
    errors: action.errors
  });
};

// GET PROJECT BY IDENTIFIER ...
const getProjectStart = state => {
  return updateObject(state, {
    projectLoading: true
  });
};
const getProjectSuccess = (state, action) => {
  return updateObject(state, {
    projectLoading: false,
    project: action.project
  });
};
const getProjectFail = (state, action) => {
  return updateObject(state, {
    projectLoading: false,
    errors: action.errors
  });
};

// DELETE PROJECT BY IDENTIFIER ...
const deleteProjectStart = state => {
  return updateObject(state, {});
};
const deleteProjectSuccess = (state, action) => {
  return updateObject(state, {});
};
const deleteProjectFail = (state, action) => {
  return updateObject(state, {
    projectLoading: false,
    errors: action.errors
  });
};

const clearErrors = state => {
  return updateObject(state, {
    errors: {}
  });
};
const ProjectReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.ADD_PROJECT_START:
      return createProjectStart(state);
    case actionTypes.ADD_PROJECT_SUCCESS:
      return createProjectSuccess(state, action);
    case actionTypes.ADD_PROJECT_FAIL:
      return createProjectFail(state, action);

    case actionTypes.UPDATE_PROJECT_START:
      return updateProjectStart(state);
    case actionTypes.UPDATE_PROJECT_SUCCESS:
      return updateProjectSuccess(state, action);
    case actionTypes.UPDATE_PROJECT_FAIL:
      return updateProjectFail(state, action);

    case actionTypes.GET_PROJECTS_START:
      return getProjectsStart(state);
    case actionTypes.GET_PROJECTS_SUCCESS:
      return getProjectsSuccess(state, action);
    case actionTypes.GET_PROJECTS_FAIL:
      return getProjectsFail(state, action);

    case actionTypes.GET_PROJECT_START:
      return getProjectStart(state);
    case actionTypes.GET_PROJECT_SUCCESS:
      return getProjectSuccess(state, action);
    case actionTypes.GET_PROJECT_FAIL:
      return getProjectFail(state, action);

    case actionTypes.DELETE_PROJECT_START:
      return deleteProjectStart(state);
    case actionTypes.DELETE_PROJECT_SUCCESS:
      return deleteProjectSuccess(state, action);
    case actionTypes.DELETE_PROJECT_FAIL:
      return deleteProjectFail(state, action);

    case actionTypes.CLEAR_PROJECT_ERRORS:
      return clearErrors(state);

    default:
      return state;
  }
};

export default ProjectReducer;
