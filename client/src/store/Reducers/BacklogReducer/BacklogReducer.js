import { updateObject } from "../../../Util/Util";
import { actionTypes } from "../../Actions/ActionTypes";

const initialState = {
  projectTasks: [],
  projectTask: {},
  errors: {},
  backlogLoading: false
};

const createProjectTaskStart = state => {
  return updateObject(state, {
    backlogLoading: true
  });
};
const createProjectTaskSuccess = (state, action) => {
  return updateObject(state, {
    projectTask: action.data,
    backlogLoading: false
  });
};
const createProjectTaskFail = (state, action) => {
  return updateObject(state, {
    errors: action.errors,
    backlogLoading: false
  });
};

const updateProjectTaskStart = state => {
  return updateObject(state, {
    backlogLoading: true
  });
};
const updateProjectTaskSuccess = (state, action) => {
  return updateObject(state, {
    projectTask: action.data,
    backlogLoading: false
  });
};
const updateProjectTaskFail = (state, action) => {
  return updateObject(state, {
    errors: action.errors,
    backlogLoading: false
  });
};

const getProjectTasksStart = state => {
  return updateObject(state, {
    backlogLoading: true
  });
};
const getProjectTasksSuccess = (state, action) => {
  return updateObject(state, {
    projectTasks: action.data,
    backlogLoading: false
  });
};
const getProjectTasksFail = (state, action) => {
  return updateObject(state, {
    errors: action.errors,
    backlogLoading: false
  });
};

const getProjectTaskStart = state => {
  return updateObject(state, {
    backlogLoading: true
  });
};
const getProjectTaskSuccess = (state, action) => {
  return updateObject(state, {
    projectTask: action.data,
    backlogLoading: false
  });
};
const getProjectTaskFail = (state, action) => {
  return updateObject(state, {
    errors: action.errors,
    backlogLoading: false
  });
};

const deleteProjectTask = (state, action) => {
  return updateObject(state, {
    projectTasks: state.projectTasks.filter(
      projectTask => projectTask.projectSequence !== action.projectSequence
    )
  });
};
const clearBacklogErrors = state => {
  return updateObject(state, {
    errors: {}
  });
};
const BacklogReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.CREATE_PROJECT_TASK_START:
      return createProjectTaskStart(state);
    case actionTypes.CREATE_PROJECT_TASK_SUCCESS:
      return createProjectTaskSuccess(state, action);
    case actionTypes.CREATE_PROJECT_TASK_FAIL:
      return createProjectTaskFail(state, action);

    case actionTypes.UPDATE_PROJECT_TASK_START:
      return updateProjectTaskStart(state);
    case actionTypes.UPDATE_PROJECT_TASK_SUCCESS:
      return updateProjectTaskSuccess(state, action);
    case actionTypes.UPDATE_PROJECT_TASK_FAIL:
      return updateProjectTaskFail(state, action);

    case actionTypes.GET_PROJECT_TASKS_START:
      return getProjectTasksStart(state);
    case actionTypes.GET_PROJECT_TASKS_SUCCESS:
      return getProjectTasksSuccess(state, action);
    case actionTypes.GET_PROJECT_TASKS_FAIL:
      return getProjectTasksFail(state, action);

    case actionTypes.GET_PROJECT_TASK_START:
      return getProjectTaskStart(state);
    case actionTypes.GET_PROJECT_TASK_SUCCESS:
      return getProjectTaskSuccess(state, action);
    case actionTypes.GET_PROJECT_TASK_FAIL:
      return getProjectTaskFail(state, action);

    case actionTypes.DELETE_PROJECT_TASK:
      return deleteProjectTask(state, action);

    case actionTypes.CLEAR_BACKLOG_ERRORS:
      return clearBacklogErrors(state);

    default:
      return state;
  }
};

export default BacklogReducer;
