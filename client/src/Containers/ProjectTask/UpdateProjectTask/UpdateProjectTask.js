import PropTypes from "prop-types";
import React, { Component } from "react";
import { Field, Form } from "react-final-form";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import Input from "../../../Components/Input/Input";
import SelectField from "../../../Components/Input/SelectField/SelectField";
import TextAreaField from "../../../Components/Input/TextAreaField/TextAreaField";
import Loader from "../../../Components/UI/Loader/Loader";
import { clearErrors, getProjectTask, updateProjectTask } from "../../../store/Actions/BacklogAction";
import { formatDate, isEmpty } from "../../../Util/Util";
import utilClasses from "../../../Util/Util.module.scss";
import addProjectTaskValidations from "../../../Validators/AddProjectTaskValidations";
import classes from "./UpdateProjectTask.module.scss";


const priorityOptions = [
  { value: "", label: "Select Priority" },
  { value: 1, label: "LOW" },
  { value: 2, label: "MEDIUM" },
  { value: 3, label: "HIGH" }
];
const statusOptions = [
  { value: "", label: "Select Status" },
  { value: "TO_DO", label: "TO_DO" },
  { value: "IN_PROGRESS", label: "IN_PROGRESS" },
  { value: "DONE", label: "DONE" }
];
class UpdateProjectTask extends Component {
  componentDidMount = () => {
    this.props.onClearErrors();
    this.props.onGetProjectTask(
      this.props.match.params.projectIdentifier,
      this.props.match.params.projectSequence
    );
  };
  handleSubmit = formValues => {
    console.log(formValues);
    this.props.onUpdateProjectTask(
      formValues,
      this.props.match.params.projectIdentifier,
      this.props.match.params.projectSequence,
      this.props.history
    );
  };

  render() {
    const { errors, backlogLoading, initialValues } = this.props;

    if (backlogLoading) {
      return (
        <div className={utilClasses.Loader__Centered}>
          <Loader />
        </div>
      );
    }
    return (
      <div className={classes.UpdateProjectTask}>
        <Link
          to={`/projectBoard/${this.props.match.params.projectIdentifier}`}
          className={utilClasses.Link}
        >
          &lArr; Back
        </Link>
        <h1
          className={utilClasses.Primary__Heading}
          style={{ justifySelf: "center" }}
        >
          Update Project Task Form
        </h1>
        <Form
          initialValues={initialValues}
          onSubmit={this.handleSubmit}
          validate={addProjectTaskValidations}
          render={({ handleSubmit, pristine, submitting }) => (
            <form
              onSubmit={handleSubmit}
              className={classes.UpdateProjectTask_Form}
            >
              <Field
                component={Input}
                name="summary"
                id="summary"
                placeholder="* Project Summary"
                label="* Project Summary"
                info="Give summary for project task"
                error={errors.summary || errors.projectNotFound}
              />
              <Field
                component={TextAreaField}
                name="acceptenceCriteria"
                id="acceptenceCriteria"
                placeholder="* Acceptance Criteria"
                label="* Acceptance Criteria"
                info="Acceptance criteria for project task"
                error={errors.acceptenceCriteria}
              />
              <Field
                component={Input}
                label="Due Date"
                name="dueDate"
                id="dueDate"
                inputType="date"
                info="Due date for project task"
              />
              <Field
                component={SelectField}
                name="priority"
                label="Priority of project task"
                options={priorityOptions}
                id="priority"
                info="Select priority for project task ,default is 3 (low)"
              />
              <Field
                component={SelectField}
                name="status"
                label="Project Task Status"
                options={statusOptions}
                id="status"
                info="Select Status for project task ,default is 'TO_DO'"
              />
              <button
                style={{ justifySelf: "center", margin: "3rem 0rem" }}
                className={utilClasses.Button}
                disabled={submitting || pristine}
              >
                Update Project
              </button>
            </form>
          )}
        />
      </div>
    );
  }
}
const mapStateToProps = state => {
  let initialValues = null;
  if (state.backlogReducer.projectTask) {
    let dueDate = null;
    if (!isEmpty(state.backlogReducer.projectTask.dueDate)) {
      dueDate = formatDate(state.backlogReducer.projectTask.dueDate);
    }
    initialValues = {
      summary: state.backlogReducer.projectTask.summary,
      acceptenceCriteria: state.backlogReducer.projectTask.acceptenceCriteria,
      priority: state.backlogReducer.projectTask.priority,
      status: state.backlogReducer.projectTask.status,
      dueDate
    };
  }
  return {
    initialValues,
    errors: state.backlogReducer.errors,
    backlogLoading: state.backlogReducer.backlogLoading
  };
};
const mapDispatchToProps = dispatch => {
  return {
    onUpdateProjectTask: (task, projectIdenitifier, projectSequence, history) =>
      dispatch(
        updateProjectTask(task, projectIdenitifier, projectSequence, history)
      ),
    onGetProjectTask: (projectIdentifier, projectSequence) =>
      dispatch(getProjectTask(projectIdentifier, projectSequence)),
    onClearErrors: () => dispatch(clearErrors())
  };
};

UpdateProjectTask.propTypes = {
  errors: PropTypes.object.isRequired,
  backlogLoading: PropTypes.bool.isRequired,
  onClearErrors: PropTypes.func.isRequired,
  onUpdateProjectTask: PropTypes.func.isRequired
};
export default UpdateProjectTask = connect(
  mapStateToProps,
  mapDispatchToProps
)(UpdateProjectTask);
