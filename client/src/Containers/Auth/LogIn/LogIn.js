import React, { Component } from "react";
import classes from "./LogIn.module.scss";
import utilClasses from "../../../Util/Util.module.scss";
import { Form, Field } from "react-final-form";
import Input from "../../../Components/Input/Input";
import { Link } from "react-router-dom";
import logInValidations from "../../../Validators/LogInValidations";
import { connect } from "react-redux";
import {
  clearAuthLogInErrors,
  authLogIn,
} from "../../../store/Actions/AuthAction";
import Loader from "../../../Components/UI/Loader/Loader";

class LogIn extends Component {
  state = {
    showHidePassword: false,
  };

  componentDidMount = () => {
    this.props.onClearAuthLogInErrors();
    // CHECK IF USER IS AUTHENTICATED OR NOT IF YES REDIRECT USER TO DASHBOARD FROM LOGIN...
    if (this.props.isAuthenticated) {
      this.props.history.push("/");
    }
  };
  componentDidUpdate = () => {
    if (this.state.showHidePassword) {
      setTimeout(() => {
        this.setState({ showHidePassword: false });
      }, 1000);
    }
  };

  somethingWentWrongCloseHandler = () => {
    this.props.onSomethingWentWrongClose();
  };

  //LOGIN FORM SUBMIT HANDLER METHOD ...
  logInFormSubmitHandler = (formValues) => {
    const userData = {
      email: formValues.email,
      password: formValues.password,
    };
    // CALLING LOGIN METHOD IN REDUX STORE...
    this.props.onAuthLogIn(userData, this.props.history);
  };

  // SHOW OR HIDE PASSWORD FOR PASSWORD FIELD SIMPLE DOM METHOD ...
  showHidePassword = () => {
    let passwordElement = document.getElementById("password");
    if (passwordElement.type === "password") {
      passwordElement.type = "text";
      setTimeout(() => {
        passwordElement.type = "password";
      }, 1000);
    }
    this.setState((prevState) => {
      return { showHidePassword: !prevState.showHidePassword };
    });
  };

  render() {
    const { authLoading, errors } = this.props;

    if (authLoading) {
      return (
        <div className={classes.LogIn}>
          <Loader />
        </div>
      );
    }
    return (
      <div className={classes.LogIn}>
        <h1
          className={`${utilClasses.Primary__Heading} ${utilClasses.Centered}`}
        >
          LogIn
        </h1>
        <Form
          onSubmit={this.logInFormSubmitHandler}
          validate={logInValidations}
          render={({
            handleSubmit,
            pristine,
            submitting,
            hasValidationErrors,
          }) => {
            return (
              <form onSubmit={handleSubmit} className={classes.LogIn_Form}>
                <Field
                  name="email"
                  component={Input}
                  label="* Enter Your Email"
                  placeholder="* Enter Your Email"
                  id="email"
                  info="Email address should contain '@' character."
                />
                <Field
                  component={Input}
                  id="password"
                  placeholder="* Enter Your Password"
                  name="password"
                  label="* Enter Your Password"
                  inputType="password"
                  showHidePassword={this.state.showHidePassword}
                  showHidePasswordFunc={this.showHidePassword}
                  error={errors.emailOrPasswordIsNotValid}
                />
                <button
                  className={utilClasses.Button}
                  style={{ justifySelf: "center" }}
                  disabled={pristine || submitting || hasValidationErrors}
                >
                  LogIn
                </button>
              </form>
            );
          }}
        />
        <p className={`${utilClasses.Paragraph}`}>
          Don't have an account ?{" "}
          <Link to="/signup" className={classes.LogInAndSignUp_Form__Link}>
            Create account here.
          </Link>
        </p>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    isAuthenticated: state.authReducer.isAuthenticated,
    authLoading: state.authReducer.authLoading,
    errors: state.authReducer.authLogInErrors,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    onClearAuthLogInErrors: () => dispatch(clearAuthLogInErrors()),
    onAuthLogIn: (userData, history) => dispatch(authLogIn(userData, history)),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(LogIn);
