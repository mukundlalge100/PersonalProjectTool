import React, { Component } from "react";
import Modal from "../Modal/Modal";
import PropTypes from "prop-types";

class ErrorBoundary extends Component {
  state = { hasError: false, showModal: true };

  closeModalHandler = () => {
    this.setState(prevState => ({
      showModal: !prevState.showModal
    }));
  };
  static getDerivedStateFromError(error) {
    return { hasError: true };
  }

  componentDidCatch = (error, errorInfo) => {
    console.log(error);
    console.log(errorInfo);
  };
  render() {
    if (this.state.hasError) {
      return (
        <Modal show={this.state.showModal} closeModal={this.closeModalHandler}>
          Something went wrong, please try again after some time!
        </Modal>
      );
    }
    return (
      <Modal show={this.state.showModal} closeModal={this.closeModalHandler}>
        {this.props.message}
      </Modal>
    );
  }
}

ErrorBoundary.propTypes = {
  message: PropTypes.string.isRequired
};
export default ErrorBoundary;
