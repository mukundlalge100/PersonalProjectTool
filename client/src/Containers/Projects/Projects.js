import React, { Component } from "react";
import classes from "./Projects.module.scss";
import PropTypes from "prop-types";
import Project from "../Project/Project";
import { connect } from "react-redux";
import { getProjects } from "../../store/Actions/ProjectAction";
import Loader from "../../Components/UI/Loader/Loader";
import utilClasses from "../../Util/Util.module.scss";
import Pagination from "react-js-pagination";
import Modal from "../Modal/Modal";

class Projects extends Component {
  state = {
    currentPage: 1,
    showModal: true
  };

  componentDidMount = () => {
    this.props.onGetProjects(this.state.currentPage - 1);
  };

  componentDidUpdate = (prevProps, prevState) => {
    if (prevState.currentPage !== this.state.currentPage) {
      this.props.onGetProjects(this.state.currentPage - 1);
    }
  };

  changePageHandler = currentPage => {
    this.setState({ currentPage });
  };

  showModalToggleHandler = () => {
    this.setState(prevState => ({ showModal: !prevState.showModal }));
  };

  render() {
    const { projectLoading, projects } = this.props;
    if (projectLoading) {
      return (
        <div className={classes.Projects}>
          <Loader />
        </div>
      );
    }
    if (projects.length <= 0) {
      return (
        <Modal
          show={this.state.showModal}
          closeModal={this.showModalToggleHandler}
        >
          <h1 className={utilClasses.Primary__Heading}>No projects found</h1>
        </Modal>
      );
    }
    const renderProjects = projects.map(project => {
      return (
        <Project
          totalPages={this.props.totalPages}
          currentPage={this.state.currentPage}
          key={project.projectIdentifier}
          project={project}
          onChangePageHandler={this.changePageHandler}
        />
      );
    });
    return (
      <div className={classes.Projects}>
        {renderProjects}
        {this.props.totalItemsCount > 4 ? (
          <Pagination
            pageRangeDisplayed={3}
            onChange={this.changePageHandler}
            innerClass={utilClasses.Pagination}
            itemsCountPerPage={this.props.itemsCountPerPage}
            activePage={this.state.currentPage}
            totalItemsCount={this.props.totalItemsCount}
            activeLinkClass={utilClasses.Pagination_LinkContainer__Link_Active}
            activeClass={utilClasses.Pagination_LinkContainer__Active}
            itemClass={utilClasses.Pagination_LinkContainer}
            linkClass={utilClasses.Pagination_LinkContainer__Link}
          />
        ) : null}
      </div>
    );
  }
}
const mapStateToProps = state => {
  return {
    projectLoading: state.projectReducer.projectLoading,
    projects: state.projectReducer.projects,
    totalPages: state.projectReducer.totalPages,
    itemsCountPerPage: state.projectReducer.itemsCountPerPage,
    totalItemsCount: state.projectReducer.totalItemsCount
  };
};
const mapDispatchToProps = dispatch => {
  return {
    onGetProjects: pageNumber => dispatch(getProjects(pageNumber))
  };
};
Projects.propTypes = {
  projects: PropTypes.array.isRequired,
  projectLoading: PropTypes.bool.isRequired,
  onGetProjects: PropTypes.func.isRequired
};

Projects = connect(mapStateToProps, mapDispatchToProps)(Projects);
export default Projects;
