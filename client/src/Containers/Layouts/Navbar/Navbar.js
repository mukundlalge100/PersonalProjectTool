import React, { Component } from "react";
import { NavLink, Link } from "react-router-dom";
import { ReactComponent as DashBoardSVG } from "../../../assets/SVG/home.svg";
import { ReactComponent as UserSVG } from "../../../assets/SVG/user.svg";
import { ReactComponent as LogInSVG } from "../../../assets/SVG/login.svg";
import { ReactComponent as Brand } from "../../../assets/SVG/PPMTool.svg";
import { ReactComponent as LogOutSVG } from "../../../assets/SVG/unlocked.svg";
import { ReactComponent as SignUpSVG } from "../../../assets/SVG/sign-up-key.svg";
import utilClasses from "../../../Util/Util.module.scss";
import classes from "./Navbar.module.scss";
import SideDrawerToggle from "../../../Components/UI/SideDrawer/SideDrawerToggle/SideDrawerToggle";
import SideDrawer from "../../../Components/UI/SideDrawer/SideDrawer";
import { connect } from "react-redux";
import { authLogOut } from "../../../store/Actions/AuthAction";
import { withRouter } from "react-router-dom";

class Navbar extends Component {
  state = {
    showHideSideDrawer: false,
  };
  authLogOut = () => {
    this.sideDrawerCloseHandler();
    this.props.onAuthLogOut(this.props.history);
  };
  showHideSideDrawer = () => {
    this.setState((prevState) => {
      return {
        showHideSideDrawer: !prevState.showHideSideDrawer,
      };
    });
  };
  sideDrawerCloseHandler = () => {
    this.setState({ showHideSideDrawer: false });
  };

  authLogOut = () => {
    this.props.onAuthLogOut(this.props.history);
    this.sideDrawerCloseHandler();
  };

  render() {
    let conditionalRoutes;
    const { isAuthenticated, user } = this.props;
    if (isAuthenticated) {
      conditionalRoutes = (
        <nav
          className={`${classes.Navbar_Nav} ${
            this.state.showHideSideDrawer
              ? classes.Navbar_SideDrawerShow
              : classes.Navbar_SideDrawerHide
          }`}
        >
          <NavLink
            to="/"
            exact={true}
            activeClassName={classes.Navbar_Brand__Active}
            className={classes.Navbar_Brand}
            onClick={this.sideDrawerCloseHandler}
          >
            <Brand className={classes.Navbar_Brand__SVG} />
            <h3 className={utilClasses.Tertiary__Heading}>
              Personal Project Management Tool
            </h3>
          </NavLink>
          <div className={classes.Navbar_Links}>
            <NavLink
              to="/dashboard"
              exact={true}
              activeClassName={classes.Navbar_Links__Link_Active}
              className={classes.Navbar_Links__Link}
              onClick={this.sideDrawerCloseHandler}
            >
              <DashBoardSVG className={classes.Navbar_Links__Link_SVG} />
              <p>Dashboard</p>
            </NavLink>

            <Link
              to="/"
              className={classes.Navbar_Links__Link}
              onClick={this.sideDrawerCloseHandler}
            >
              <UserSVG className={classes.Navbar_Links__Link_SVG} />
              <p>{user.userName}</p>
            </Link>
            <div
              className={classes.Navbar_Links__Link}
              onClick={this.authLogOut}
            >
              <LogOutSVG className={classes.Navbar_Links__Link_SVG} />
              <p>LogOut</p>
            </div>
          </div>
        </nav>
      );
    } else {
      conditionalRoutes = (
        <nav
          className={`${classes.Navbar_Nav} ${
            this.state.showHideSideDrawer
              ? classes.Navbar_SideDrawerShow
              : classes.Navbar_SideDrawerHide
          }`}
        >
          <NavLink
            exact={true}
            to="/"
            activeClassName={classes.Navbar_Links__Link_Active}
            className={classes.Navbar_Brand}
            onClick={this.sideDrawerCloseHandler}
          >
            <Brand className={classes.Navbar_Brand__SVG} />
            <h3 className={utilClasses.Tertiary__Heading}>
              Personal Project Management Tool
            </h3>
          </NavLink>
          <div className={classes.Navbar_Links}>
            <NavLink
              exact={true}
              activeClassName={classes.Navbar_Links__Link_Active}
              to="/signup"
              className={classes.Navbar_Links__Link}
              onClick={this.sideDrawerCloseHandler}
            >
              <SignUpSVG className={classes.Navbar_Links__Link_SVG} />
              <p>SignUp</p>
            </NavLink>
            <NavLink
              to="/login"
              exact={true}
              activeClassName={classes.Navbar_Links__Link_Active}
              className={classes.Navbar_Links__Link}
              onClick={this.sideDrawerCloseHandler}
            >
              <LogInSVG className={classes.Navbar_Links__Link_SVG} />
              <p>LogIn</p>
            </NavLink>
          </div>
        </nav>
      );
    }

    return (
      <div className={classes.Navbar}>
        <SideDrawerToggle sideDrawerToggle={this.showHideSideDrawer} />

        {this.state.showHideSideDrawer ? (
          <SideDrawer
            showHideSideDrawer={this.state.showHideSideDrawer}
            sideDrawerCloseHandler={this.sideDrawerCloseHandler}
          >
            {conditionalRoutes}
          </SideDrawer>
        ) : (
          conditionalRoutes
        )}
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    isAuthenticated: state.authReducer.isAuthenticated,
    user: state.authReducer.user,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    onAuthLogOut: (history) => dispatch(authLogOut(history)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Navbar));
