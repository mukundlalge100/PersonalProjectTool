export default class Circle {
  constructor(x, y, dx, dy, radius, color, c, canvas, mouse) {
    this.x = x;
    this.dy = dy;
    this.dx = dx;
    this.y = y;
    this.radius = radius;
    this.color = color;
    this.minRadius = radius;
    this.c = c;
    this.canvas = canvas;
    this.mouse = mouse;
  }
  draw() {
    this.c.beginPath();
    this.c.arc(this.x, this.y, this.radius, 0, Math.PI * 2, false);
    this.c.fillStyle = this.color;
    this.c.strokeStyle = "#f2eac2";
    this.c.fill();
    this.c.stroke();
    this.c.closePath();
  }
  update() {
    if (this.x + this.radius > this.canvas.width || this.x + this.radius < 0) {
      this.dx = -this.dx;
    }
    if (this.y + this.radius > this.canvas.height || this.y - this.radius < 0) {
      this.dy = -this.dy;
    }
    this.y += this.dy;
    this.x += this.dx;

    if (
      this.mouse.x - this.x < 50 &&
      this.mouse.x - this.x > -50 &&
      this.mouse.y - this.y < 50 &&
      this.mouse.y - this.y > -50
    ) {
      if (this.radius < 40) {
        this.radius += 1;
      }
    } else if (this.radius > this.minRadius) {
      this.radius -= 1;
    }
    this.draw();
  }
}
