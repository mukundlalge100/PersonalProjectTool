import React from "react";
import classes from "./Loader.module.scss";
import { ReactComponent as Spinner } from "../../../assets/SVG/Spinner.svg";

//UI LOADER FUNCTIONAL COMPONENT...
const Loader = () => {
  return <Spinner className={classes.Loader_SVG} />;
};

export default Loader;
