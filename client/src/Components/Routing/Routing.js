import React, { lazy } from "react";
import { Switch, Route, Redirect, HashRouter } from "react-router-dom";
import PageNotFound from "../PageNotFound/PageNotFound";
import Dashboard from "../../Containers/Dashboard/Dashboard";
import ProjectBoard from "../../Containers/ProjectBoard/ProjectBoard";
import UpdateProjectTask from "../../Containers/ProjectTask/UpdateProjectTask/UpdateProjectTask";
import Landing from "../Layouts/Landing/Landing";
const AddProjectTask = lazy(() =>
  import("../../Containers/ProjectTask/AddProjectTask/AddProjectTask")
);

const SignUp = lazy(() => import("../../Containers/Auth/SignUp/SignUp"));
const LogIn = lazy(() => import("../../Containers/Auth/LogIn/LogIn"));

const AddProject = lazy(() =>
  import("../../Containers/Project/AddProject/AddProject")
);
const EditProject = lazy(() =>
  import("../../Containers/Project/EditProject/EditProject")
);

const Routing = (props) => {
  let routes;
  if (props.isAuthenticated) {
    routes = (
      // PRIVATE ROUTES ...
      <Switch>
        <Route exact path="/dashboard" component={Dashboard} />
        <Route exact path="/addProject" component={AddProject} />
        <Route
          exact
          path="/projectBoard/:projectIdentifier"
          component={ProjectBoard}
        />
        <Route
          exact
          path="/projectBoard/:projectIdentifier/:projectSequence"
          component={UpdateProjectTask}
        />
        <Route
          path="/addProjectTask/:projectIdentifier"
          component={AddProjectTask}
        />
        <Route exact path="/updateProject/:projectId" component={EditProject} />
        <Route exact path="/pageNotFound" component={PageNotFound} />
        <Route exact path="/" component={Dashboard} />
      </Switch>
    );
  } else {
    routes = (
      // PUBLIC ROUTES ...
      <Switch>
        <Route exact path="/" component={Landing} />
        <Route exact path="/signup" component={SignUp} />
        <Route exact path="/login" component={LogIn} />
        <Redirect to="/" />
      </Switch>
    );
  }
  return <React.Fragment>{routes}</React.Fragment>;
};

export default Routing;
