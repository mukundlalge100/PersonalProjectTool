package com.mukund.PersonalProjectTool.request;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import lombok.Data;

@Data
public class SignUpRequest {

  @Email(message = "Email is not valid!")
  @NotBlank(message = "Email is required!")
  private String email;

  @NotBlank(message = "User Name is required!")
  @Size(min = 4, max = 30, message = "User name should be minimum 4 and maximum 30 characters!")
  private String userName;

  @NotBlank(message = "Password is required!")
  @Size(min = 8, max = 64, message = "Password should be minimum 8 and maximum 64 characters!")
  private String password;

  @NotBlank(message = "Confirm Password is required!")
  private String confirmPassword;

  @Override
  public String toString() {
    return "User [ConfirmPassword=" + confirmPassword + ", email=" + email + ", password=" + password + ", userName="
        + userName + "]";
  }
}