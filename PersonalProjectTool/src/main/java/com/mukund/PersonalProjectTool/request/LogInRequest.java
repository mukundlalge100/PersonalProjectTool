package com.mukund.PersonalProjectTool.request;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

import lombok.Data;

/**
 * LogInRequest
 */
@Data
public class LogInRequest {

  @Email(message = "Email is not valid!")
  @NotBlank(message = "Email is required!")
  private String email;

  @NotBlank(message = "Password is required!")
  private String password;

  @Override
  public String toString() {
    return "LogInRequest [email=" + email + ", password=" + password + "]";
  }

}