package com.mukund.PersonalProjectTool.request;

import java.util.Date;

import javax.validation.constraints.NotBlank;

import lombok.Data;

/**
 * PROJECTTASKDTO ...
 */

@Data
public class ProjectTaskRequest {

  private String acceptenceCriteria;

  private String status;

  private Integer priority;

  @NotBlank(message = "Summary is required!")
  private String summary;

  private String projectSequence;

  private Date dueDate;

  private String projectIdentifier;

  public ProjectTaskRequest() {
  }

  @Override
  public String toString() {
    return "ProjectTaskRequest [acceptenceCriteria=" + acceptenceCriteria + ", dueDate=" + dueDate + ", priority="
        + priority + ", projectIdentifier=" + projectIdentifier + ", projectSequence=" + projectSequence + ", status="
        + status + ", summary=" + summary + "]";
  }
}