package com.mukund.PersonalProjectTool.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

/**
 * Backlog
 */
@Data
@Entity
@Table(name = "backlog")
public class Backlog {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer backlogId;

  private Integer PTSequence = 0;

  @Column(name = "projectIdentifier", updatable = false)
  private String projectIdentifier;

  // ONE TO ONE W PROJECT ...
  @OneToOne
  @JoinColumn(name = "projectId")
  @JsonIgnore
  private Project project;

  // ONE TO MANY WITH PROJECTTASK ...

  @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "backlog")
  private List<ProjectTask> projectTasks = new ArrayList<>();

  @Override
  public String toString() {
    return "{" + " backlogId='" + getBacklogId() + "'" + ", PTSequence='" + getPTSequence() + "'"
        + ", projectIdentifier='" + getProjectIdentifier() + "'" + "}";
  }

}