package com.mukund.PersonalProjectTool.entities;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

/**
 * PROJECTTASK ENTITY
 */

@Data
@Entity
@Table(name = "projecttask")
public class ProjectTask {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer projectTaskId;

  private String acceptenceCriteria;

  private String status;

  private Integer priority;

  private String summary;

  @Column(updatable = false, unique = true)
  private String projectSequence;

  @Column(updatable = false)
  private String projectIdentifier;

  private LocalDateTime dueDate;

  @JsonIgnore
  @Column(updatable = false)
  private LocalDateTime createdAt;

  @JsonIgnore
  private LocalDateTime updatedAt;

  // MANY TO ONE RELATIONSHIP WITH BACKLOG ...
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "backlogId")
  @JsonIgnore
  private Backlog backlog;

  @PrePersist
  protected void onCreate() {
    this.createdAt = LocalDateTime.now();
  }

  @PreUpdate
  protected void onUpdate() {
    this.updatedAt = LocalDateTime.now();
  }

  @Override
  public String toString() {
    return "ProjectTask [acceptenceCriteria=" + acceptenceCriteria + ", backlog=" + backlog + ", createdAt=" + createdAt
        + ", dueDate=" + dueDate + ", priority=" + priority + ", projectIdentifier=" + projectIdentifier
        + ", projectSequence=" + projectSequence + ", projectTaskId=" + projectTaskId + ", status=" + status
        + ", summary=" + summary + ", updatedAt=" + updatedAt + "]";
  }

}