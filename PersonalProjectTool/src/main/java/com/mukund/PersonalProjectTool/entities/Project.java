package com.mukund.PersonalProjectTool.entities;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Data
@Entity
public class Project {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer projectId;

	private String projectName;

	@Column(updatable = false, unique = true)
	private String projectIdentifier;

	private String description;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	private Date startDate;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	private Date endDate;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	private Date updatedAt;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	@Column(updatable = false)
	private Date createdAt;

	// ONE TO ONE MAPPING WITH BACKLOG ...
	@OneToOne(cascade = { CascadeType.ALL }, mappedBy = "project", fetch = FetchType.LAZY)
	@JsonIgnore
	private Backlog backlog;

	// MANY TO ONE WITH USER ...
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "userId")
	@JsonIgnore
	private User user;

	private String projectLeader;

	@PrePersist
	protected void onCreate() {
		this.createdAt = new Date();
	}

	@PreUpdate
	protected void onUpdate() {
		this.updatedAt = new Date();
	}

	@Override
	public String toString() {
		return "Project [backlog=" + backlog + ", createdAt=" + createdAt + ", description=" + description + ", endDate="
				+ endDate + ", projectId=" + projectId + ", projectIdentifier=" + projectIdentifier + ", projectLeader="
				+ projectLeader + ", projectName=" + projectName + ", startDate=" + startDate + ", updatedAt=" + updatedAt
				+ ", user=" + user + "]";
	}

}
