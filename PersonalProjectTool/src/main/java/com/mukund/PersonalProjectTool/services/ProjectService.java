package com.mukund.PersonalProjectTool.services;

import java.text.ParseException;

import com.mukund.PersonalProjectTool.Util.Util;
import com.mukund.PersonalProjectTool.entities.Backlog;
import com.mukund.PersonalProjectTool.entities.Project;
import com.mukund.PersonalProjectTool.entities.User;
import com.mukund.PersonalProjectTool.exception.ProjectException.ProjectIdentifierException;
import com.mukund.PersonalProjectTool.exception.ProjectException.ProjectNotFoundException;
import com.mukund.PersonalProjectTool.mappers.ProjectMapper;
import com.mukund.PersonalProjectTool.repositories.BacklogRepository;
import com.mukund.PersonalProjectTool.repositories.ProjectRepository;
import com.mukund.PersonalProjectTool.repositories.UserRepository;
import com.mukund.PersonalProjectTool.request_response.ProjectDto;
import com.mukund.PersonalProjectTool.response.CheckProjectIdentifierExistResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 * Service for Project
 */
@Service
public class ProjectService {

	@Autowired
	private ProjectRepository projectRepository;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private BacklogRepository backlogRepository;

	@Autowired
	private Util utilService;

	private Logger LOGGER = LoggerFactory.getLogger(ProjectService.class);

	/**
	 * Service method for creating project
	 * 
	 * @param projectDto - {@link ProjectDto}
	 * @return projectDto {@link ProjectDto}
	 */
	public ProjectDto createProject(ProjectDto projectDto) {
		LOGGER.info("ProjectService -> createProject -> STARTS");
		User user = userRepository.findUserByEmail(utilService.getLoggedInUserName());
		projectDto.setProjectIdentifier(projectDto.getProjectIdentifier().toUpperCase());
		// CHECK IF PROJECT IDENTIFIER IS ALREADY EXISTS OR NOT ...
		Project project = projectRepository.findByProjectIdentifier(projectDto.getProjectIdentifier());
		if (project != null) {
			throw new ProjectIdentifierException("Project ID " + projectDto.getProjectIdentifier() + " is already exists!");
		}
		// DTO TO ENTITY ...
		Project project2 = ProjectMapper.INSTANCE.projectDtoToProject(projectDto);
		Backlog backlog = new Backlog();
		project2.setBacklog(backlog);
		project2.setProjectLeader(utilService.getLoggedInUserName());
		project2.setUser(user);
		backlog.setProject(project2);
		backlog.setProjectIdentifier(projectDto.getProjectIdentifier());
		Project project3 = projectRepository.save(project2);
		LOGGER.info("ProjectService -> createProject -> ENDS");
		return ProjectMapper.INSTANCE.projectToProjectDto(project3);
	}

	/**
	 * Service for update project
	 * 
	 * @param projectDto        - {@link ProjectDto}
	 * @param projectIdentifier - {@link String}
	 * @return projectDto - {@link ProjectDto}
	 */
	public ProjectDto updateProject(ProjectDto projectDto, String projectIdentifier) {
		Project project = projectRepository.findByProjectIdentifier(projectIdentifier);
		if (project == null) {
			throw new ProjectNotFoundException("Project not found!");
		}
		if (!project.getProjectLeader().equals(utilService.getLoggedInUserName())) {
			throw new ProjectNotFoundException("Project not found with your account!");
		}
		Backlog backlog = backlogRepository.findByProjectIdentifier(projectIdentifier);
		project.setBacklog(backlog);
		project.setProjectName(projectDto.getProjectName());
		project.setDescription(projectDto.getDescription());
		project.setStartDate(projectDto.getStartDate());
		project.setEndDate(projectDto.getEndDate());
		Project project2 = projectRepository.save(project);
		return ProjectMapper.INSTANCE.projectToProjectDto(project2);
	}

	/**
	 * Service for finding project by projectIdentifier
	 * 
	 * @param projectIdentifier - {@link String}
	 * @return projectDto - {@link ProjectDto}
	 * @throws ParseException
	 */
	public ProjectDto findProjectByProjectIdentifier(String projectIdentifier) throws ParseException {
		Project project = projectRepository.findByProjectIdentifier(projectIdentifier.toUpperCase());
		if (project == null) {
			throw new ProjectIdentifierException(
					"Project ID " + projectIdentifier + " doesn't exist,please provide valid " + "project identifier!");
		}
		if (!project.getProjectLeader().equals(utilService.getLoggedInUserName())) {
			throw new ProjectNotFoundException("Project not found with your account!");
		}
		return ProjectMapper.INSTANCE.projectToProjectDto(project);
	}

	/**
	 * Service getting all projects with pagination
	 * 
	 * @param pageable - {@link Pageable}
	 * @return Page < ProjectDto >
	 */
	public Page<ProjectDto> findAllProjects(Pageable pageable) {
		Page<Project> projects = projectRepository.findByProjectLeader(pageable, utilService.getLoggedInUserName());
		return projects.map(project -> ProjectMapper.INSTANCE.projectToProjectDto(project));
	}

	// DELETE PROJECT BY PROJECT IDENTIFIER ...
	public void deleteProjectByProjectIdentifier(String projectIdentifier) {
		Project project = projectRepository.findByProjectIdentifier(projectIdentifier);
		if (project == null) {
			throw new ProjectIdentifierException("Project identifier " + projectIdentifier + " does not exists!!");
		}
		if (!project.getProjectLeader().equals(utilService.getLoggedInUserName())) {
			throw new ProjectNotFoundException("Project not found with your account!");
		}
		projectRepository.delete(project);
	}

	/**
	 * Service for checking if project is exists or not
	 * 
	 * @param projectIdentifier
	 * @return CheckProjectIdentifierExistResponse
	 */
	public CheckProjectIdentifierExistResponse checkProjectIdentifierExists(String projectIdentifier) {
		Project project = projectRepository.findByProjectIdentifier(projectIdentifier);
		CheckProjectIdentifierExistResponse checkProjectIdentifierExistResponse = new CheckProjectIdentifierExistResponse();
		if (project != null) {
			checkProjectIdentifierExistResponse.setMessage("Project has found");
			checkProjectIdentifierExistResponse.setSuccess(true);
		} else {
			throw new ProjectNotFoundException("Project has not found");
		}
		return checkProjectIdentifierExistResponse;
	}
}
