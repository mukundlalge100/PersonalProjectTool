package com.mukund.PersonalProjectTool.services;

import java.util.Objects;

import com.mukund.PersonalProjectTool.constants.Messages;
import com.mukund.PersonalProjectTool.Util.Util;
import com.mukund.PersonalProjectTool.entities.Backlog;
import com.mukund.PersonalProjectTool.entities.Project;
import com.mukund.PersonalProjectTool.entities.ProjectTask;
import com.mukund.PersonalProjectTool.exception.BacklogException.ProjectTaskNotFoundException;
import com.mukund.PersonalProjectTool.exception.ProjectException.ProjectNotFoundException;
import com.mukund.PersonalProjectTool.mappers.ProjectTaskMapper;
import com.mukund.PersonalProjectTool.repositories.BacklogRepository;
import com.mukund.PersonalProjectTool.repositories.ProjectRepository;
import com.mukund.PersonalProjectTool.repositories.ProjectTaskRepository;
import com.mukund.PersonalProjectTool.request_response.ProjectTaskDto;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 
 */
@Service
public class BacklogService {

  @Autowired
  private BacklogRepository backlogRepository;

  @Autowired
  private ProjectTaskRepository projectTaskRepository;

  @Autowired
  private ProjectRepository projectRepository;

  @Autowired
  private Util utilService;

  private Logger LOGGER = LoggerFactory.getLogger(BacklogService.class);

  /**
   * Checking user is belong to project or not
   * 
   * @param projectIdentifier {@link String}
   * @return void
   */
  private void checkProjectForUser(String projectIdentifier) {
    LOGGER.info("BacklogService -> checkProjectForUser -> STARTS");
    Project project = projectRepository.findByProjectIdentifier(projectIdentifier);

    if (Objects.isNull(project)) {
      throw new ProjectNotFoundException(String.format(Messages.PROJECT_NOT_FOUND_WITH_ID, projectIdentifier));
    }

    if (!utilService.getLoggedInUserName().equals(project.getProjectLeader())) {
      throw new ProjectNotFoundException(Messages.PROJECT_NOT_FOUND_ACCOUNT);
    }
    LOGGER.info("BacklogService -> checkProjectForUser -> ENDS");
  }

  /**
   * For creating projectTask service method
   * 
   * @param projectIdentifier {@link String}
   * @param projectTaskDto    {@link ProjectTaskDto}
   * @return projectTaskDto {@link ProjectTaskDto}
   */
  public ProjectTaskDto createProjectTask(String projectIdentifier, ProjectTaskDto projectTaskDto) {
    LOGGER.info("BacklogService -> createProjectTask -> STARTS");
    checkProjectForUser(projectIdentifier);
    // CHECK IF BACKLOG IS PRESENT OR NOT FOR PROJECT ...
    Backlog backlog = backlogRepository.findByProjectIdentifier(projectIdentifier);
    if (backlog == null) {
      throw new ProjectNotFoundException(String.format(Messages.PROJECT_NOT_FOUND_WITH_ID, projectIdentifier));
    }
    ProjectTask projectTask = ProjectTaskMapper.INSTANCE.projectTaskDtoToProjectTask(projectTaskDto);
    // PROJECT SEQUENCE SHOULD BE LIKE THIS ID009-1 ID009-2 etc ...
    Integer backlogSequence = backlog.getPTSequence();
    backlogSequence++;
    backlog.setPTSequence(backlogSequence);
    // SAVE BACKLOG AGAIN ...
    backlogRepository.save(backlog);
    projectTask.setProjectSequence(projectIdentifier + "-" + backlogSequence);
    projectTask.setAcceptenceCriteria(projectTaskDto.getAcceptenceCriteria());
    // SET BACKLOG TO PROJECT TASK ...
    projectTask.setBacklog(backlog);
    // SET PROJECT IDENTIFIER TO PROJECT TASK ...
    projectTask.setProjectIdentifier(projectIdentifier);
    // SET DEFAULT PRIORITY OF TASK ...
    if (projectTask.getPriority() == null) {
      projectTask.setPriority(3);
    }
    // DEFAULT STATUS OF PROJECT TASK ...
    if (projectTask.getStatus() == "" || projectTask.getStatus() == null) {
      projectTask.setStatus("TO_DO");
    }
    LOGGER.info("BacklogService -> createProjectTask -> ENDS");
    return ProjectTaskMapper.INSTANCE.prokectTaskToProjectTaskDto(projectTaskRepository.save(projectTask));
  }

  /**
   * Service for getting all projects
   * 
   * @param projectIdentifier
   * @return {@link Iterable}< {@link ProjectTaskDto} >
   *
   */
  public Iterable<ProjectTaskDto> getAllProjectTasks(String projectIdentifier) {
    LOGGER.info("BacklogService -> getAllProjectTasks -> STARTS");
    checkProjectForUser(projectIdentifier);
    Backlog backlog = backlogRepository.findByProjectIdentifier(projectIdentifier);
    if (backlog == null) {
      throw new ProjectNotFoundException(String.format(Messages.PROJECT_NOT_FOUND_WITH_ID, projectIdentifier));
    }
    Iterable<ProjectTask> projectTasks = projectTaskRepository
        .findByProjectIdentifierOrderByPriorityDesc(projectIdentifier);
    LOGGER.info("BacklogService -> getAllProjectTasks -> ENDS");
    return ProjectTaskMapper.INSTANCE.convert(projectTasks);
  }

  /**
   * Service for getting project task by project sequence
   * 
   * @param projectIdentifier - {@link String}
   * @param projectSequence   - {@link String}
   * @return projectTask - {@link ProjectTask}
   */
  public ProjectTask getProjectTaskByProjectSequence(String projectIdentifier, String projectSequence) {
    LOGGER.info("BacklogService -> getProjectTaskByProjectSequence -> STARTS");
    checkProjectForUser(projectIdentifier);
    Backlog backlog = backlogRepository.findByProjectIdentifier(projectIdentifier);
    // CHECK IF PROJECT IS VALID OR NOT...
    if (backlog == null) {
      throw new ProjectNotFoundException(String.format(Messages.PROJECT_NOT_FOUND_WITH_ID, projectIdentifier));
    }
    ProjectTask projectTask = projectTaskRepository.findByProjectSequence(projectSequence);
    // CHECK IF PROJECT TASK IS VALID OR NOT ...
    if (projectTask == null) {
      throw new ProjectTaskNotFoundException(String.format(Messages.PROJECT_TASK_NOT_FOUND, projectSequence));
    }
    // CHECK IF PROJECT TASK BELONGS TO THAT SPECIFIC PROJECT...
    if (!projectTask.getProjectIdentifier().equalsIgnoreCase(projectIdentifier)) {
      throw new ProjectTaskNotFoundException(
          String.format(Messages.PROJECT_TASK_NOT_FOUND_WITH_PROJECT, projectSequence, projectIdentifier));
    }
    LOGGER.info("BacklogService -> getProjectTaskByProjectSequence -> ENDS");
    return projectTask;
  }

  /**
   * Service for updating project task
   * 
   * @param projectTaskDto    - {@link ProjectService}
   * @param projectIdentifier - {@link String}
   * @param projectSequence   - {@link String}
   * @return projectTaskDto - {@link ProjectTaskDto}
   */
  public ProjectTaskDto updateProjectTask(ProjectTaskDto projectTaskDto, String projectIdentifier,
      String projectSequence) {
    LOGGER.info("BacklogService -> updateProjectTask -> STARTS");
    checkProjectForUser(projectIdentifier);
    ProjectTask projectTask = getProjectTaskByProjectSequence(projectIdentifier, projectSequence);
    projectTask.setSummary(projectTaskDto.getSummary());
    projectTask.setAcceptenceCriteria(projectTaskDto.getAcceptenceCriteria());
    projectTask.setDueDate(projectTaskDto.getDueDate());
    projectTask.setPriority(projectTaskDto.getPriority());
    projectTask.setStatus(projectTaskDto.getStatus());
    LOGGER.info("BacklogService -> updateProjectTask -> ENDS");
    return ProjectTaskMapper.INSTANCE.prokectTaskToProjectTaskDto(projectTaskRepository.save(projectTask));
  }

  /**
   * Service for deleting project task
   * 
   * @param projectIdentifier - {@link String}
   * @param projectSequence   - {@link String}
   * @return void
   */
  public void deleteProjectTask(String projectIdentifier, String projectSequence) {
    LOGGER.info("BacklogService -> deleteProjectTask -> STARTS");
    checkProjectForUser(projectIdentifier);
    ProjectTask projectTask = getProjectTaskByProjectSequence(projectIdentifier, projectSequence);
    projectTaskRepository.delete(projectTask);
    LOGGER.info("BacklogService -> deleteProjectTask -> ENDS");
  }
}
