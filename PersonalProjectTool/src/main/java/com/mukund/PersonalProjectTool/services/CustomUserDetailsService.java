package com.mukund.PersonalProjectTool.services;

import java.util.Optional;

import com.mukund.PersonalProjectTool.entities.User;
import com.mukund.PersonalProjectTool.repositories.UserRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * CustomUserDetailsService
 */

@Service
public class CustomUserDetailsService implements UserDetailsService {

  @Autowired
  private UserRepository userRepository;

  private Logger LOGGER = LoggerFactory.getLogger(CustomUserDetailsService.class);

  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    LOGGER.info("CustomUserDetailsService -> loadUserByUsername -> STARTS");
    User user = userRepository.findUserByEmail(username);
    if (user == null)
      new UsernameNotFoundException("User not found!");

    LOGGER.info("CustomUserDetailsService -> loadUserByUsername -> ENDS");
    return user;
  }

  @Transactional
  public User findUserById(Long userId) {
    LOGGER.info("CustomUserDetailsService -> findUserById -> STARTS");
    Optional<User> userOptional = userRepository.findById(userId);
    if (!userOptional.isPresent()) {
      new UsernameNotFoundException("User not found!");
    }
    LOGGER.info("CustomUserDetailsService -> findUserById -> ENDS");
    return userOptional.get();
  }

}