package com.mukund.PersonalProjectTool.services;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mukund.PersonalProjectTool.request.LogInRequest;
import com.mukund.PersonalProjectTool.response.LogInResponse;
import com.mukund.PersonalProjectTool.response.SignUpResponse;
import com.mukund.PersonalProjectTool.constants.Messages;
import com.mukund.PersonalProjectTool.entities.User;
import com.mukund.PersonalProjectTool.exception.InternalServerException;
import com.mukund.PersonalProjectTool.exception.UserException.EmailAlreadyExistsException;
import com.mukund.PersonalProjectTool.mappers.UserMapper;
import com.mukund.PersonalProjectTool.repositories.UserRepository;
import com.mukund.PersonalProjectTool.request.SignUpRequest;
import com.mukund.PersonalProjectTool.security.JwtTokenProvider;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Service;

/**
 * UserService
 */

@Service
public class UserService {

  @Autowired
  private BCryptPasswordEncoder bCryptPasswordEncoder;

  @Autowired
  private JwtTokenProvider jwtTokenProvider;

  @Autowired
  private AuthenticationManager authManager;

  @Autowired
  private UserRepository userRepository;

  /**
   * Service for login functionality
   * 
   * @return ResponseEntity < ? >
   * @param logInRequest - {@link LogInRequest}
   */
  public ResponseEntity<?> logIn(LogInRequest logInRequest) {
    Authentication authentication = authManager
        .authenticate(new UsernamePasswordAuthenticationToken(logInRequest.getEmail(), logInRequest.getPassword()));
    SecurityContextHolder.getContext().setAuthentication(authentication);
    String jwtToken = jwtTokenProvider.generateToken(authentication);
    return ResponseEntity.ok(new LogInResponse(true, jwtToken));
  }

  /**
   * Service for signup functionality
   * 
   * @param signUpRequest - {@link SignUpRequest}
   * @return signUpResponse - {@link SignUpResponse}
   */
  public SignUpResponse createUser(SignUpRequest signUpRequest) {
    try {
      // CHECK IF USER IS ALREADY EXISTS OR NOT ...
      User user = userRepository.findUserByEmail(signUpRequest.getEmail());
      if (user != null) {
        throw new EmailAlreadyExistsException("Email is already taken,please try different email!");
      }
      User user2 = UserMapper.INSTANCE.SignUpRequestToUser(signUpRequest);
      user2.setPassword(bCryptPasswordEncoder.encode(signUpRequest.getPassword()));
      SignUpResponse signUpResponse = UserMapper.INSTANCE.userToSignUpResponse(userRepository.save(user2));
      signUpResponse.setMessage(Messages.SIGNUP_SUCCESSFULLY);
      signUpResponse.setSuccess(Boolean.TRUE);
      return signUpResponse;
    } catch (Exception e) {
      throw new InternalServerException(Messages.SIGNUP_FAILED);
    }

  }

  /**
   * Service for logout functionality
   * 
   * @param request  - {@link HttpServletRequest}
   * @param response - {@link HttpServletResponse}
   * @return ResponseEntity < ? >
   */
  public ResponseEntity<?> logOut(HttpServletRequest request, HttpServletResponse response) {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    Map<String, Object> map = new HashMap<>();
    if (auth != null) {
      new SecurityContextLogoutHandler().logout(request, response, auth);
      map.put("success", true);
      map.put("message", "Logout Successfully!");
    }

    return new ResponseEntity<Map<String, Object>>(map, HttpStatus.ACCEPTED);
  }
}