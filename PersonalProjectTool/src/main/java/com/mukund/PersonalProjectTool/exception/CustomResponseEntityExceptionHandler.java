package com.mukund.PersonalProjectTool.exception;

import java.time.LocalDateTime;

import com.mukund.PersonalProjectTool.exception.BacklogException.ProjectTaskNotFoundException;
import com.mukund.PersonalProjectTool.exception.BacklogException.ProjectTaskNotFoundExceptionResponse;
import com.mukund.PersonalProjectTool.exception.ProjectException.ProjectIdentifierException;
import com.mukund.PersonalProjectTool.exception.ProjectException.ProjectIdentifierExeptionResponse;
import com.mukund.PersonalProjectTool.exception.ProjectException.ProjectNotFoundException;
import com.mukund.PersonalProjectTool.exception.ProjectException.ProjectNotFoundExceptionResponse;
import com.mukund.PersonalProjectTool.exception.UserException.EmailAlreadyExistsException;
import com.mukund.PersonalProjectTool.exception.UserException.EmailAlreadyExistsResponse;
import com.mukund.PersonalProjectTool.exception.UserException.InvalidLogInException;
import com.mukund.PersonalProjectTool.exception.UserException.InvalidLogInResponse;
import com.mukund.PersonalProjectTool.response.InternalServerExceptionResponse;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestController
@RestControllerAdvice
public class CustomResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(value = ProjectIdentifierException.class)
	public ResponseEntity<Object> handlerProjectIdentifierException(ProjectIdentifierException ex, WebRequest request) {
		ProjectIdentifierExeptionResponse response = new ProjectIdentifierExeptionResponse();
		response.setProjectIdentifier(ex.getMessage());
		return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(value = ProjectNotFoundException.class)
	public ResponseEntity<Object> handleProjectNotFoundException(ProjectNotFoundException ex, WebRequest request) {
		ProjectNotFoundExceptionResponse response = new ProjectNotFoundExceptionResponse();
		response.setProjectNotFound(ex.getMessage());
		response.setSuccess(false);
		return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
	}

	@ExceptionHandler(value = ProjectTaskNotFoundException.class)
	public ResponseEntity<Object> handleProjectTaskNotFoundException(ProjectTaskNotFoundException ex,
			WebRequest request) {
		ProjectTaskNotFoundExceptionResponse response = new ProjectTaskNotFoundExceptionResponse();
		response.setProjectTaskNotFound(ex.getMessage());
		return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
	}

	@ExceptionHandler(value = EmailAlreadyExistsException.class)
	public ResponseEntity<Object> handleEmailAlreadyExistsException(EmailAlreadyExistsException ex, WebRequest request) {
		EmailAlreadyExistsResponse response = new EmailAlreadyExistsResponse();
		response.setEmail(ex.getMessage());
		return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(value = InvalidLogInException.class)
	public ResponseEntity<Object> handleInvalidLogInException(InvalidLogInException ex, WebRequest request) {
		InvalidLogInResponse response = new InvalidLogInResponse();
		response.setEmailOrPasswordIsNotValid(ex.getMessage());
		return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(value = InternalServerException.class)
	public ResponseEntity<Object> handleInternalServerException(InternalServerException ex, WebRequest request) {
		InternalServerExceptionResponse exceptionResponse = new InternalServerExceptionResponse();
		exceptionResponse.setError("Internal Server Exception");
		exceptionResponse.setMessage(ex.getMessage());
		exceptionResponse.setPath(request.getContextPath());
		exceptionResponse.setStatus(Long.valueOf(HttpStatus.INTERNAL_SERVER_ERROR.value()));
		exceptionResponse.setTimeStamp(LocalDateTime.now());
		return new ResponseEntity<>(exceptionResponse, HttpStatus.INTERNAL_SERVER_ERROR);
	}

}
