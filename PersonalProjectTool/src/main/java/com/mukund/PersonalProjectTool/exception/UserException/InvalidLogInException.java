package com.mukund.PersonalProjectTool.exception.UserException;

/**
 * InvalidLogInException
 */
public class InvalidLogInException extends RuntimeException {

  private static final long serialVersionUID = 1L;

  public InvalidLogInException(String message) {
    super(message);
  }

}