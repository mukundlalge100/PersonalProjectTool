package com.mukund.PersonalProjectTool.exception.UserException;

import lombok.Data;

/**
 * EmailAlreadyExistsResponse
 */
@Data
public class EmailAlreadyExistsResponse {
  private String email;
}