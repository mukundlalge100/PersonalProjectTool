package com.mukund.PersonalProjectTool.exception.JwtException;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.http.HttpStatus;
import org.springframework.web.filter.OncePerRequestFilter;

import io.jsonwebtoken.JwtException;

class JwtExceptionHandler extends OncePerRequestFilter {
  @Override
  public void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
      throws ServletException, IOException {
    try {
      filterChain.doFilter(request, response);
    } catch (JwtException e) {
      JwtExceptionResponse jwtExceptionResponse = new JwtExceptionResponse();
      jwtExceptionResponse.setMessage(e.getMessage());
      jwtExceptionResponse.setSuccess(false);
      response.setStatus(HttpStatus.BAD_REQUEST.value());
      response.getWriter().write(convertObjectToJson(jwtExceptionResponse));
    }
  }

  private String convertObjectToJson(Object object) throws JsonProcessingException {
    if (object == null) {
      return null;
    }
    ObjectMapper mapper = new ObjectMapper();
    return mapper.writeValueAsString(object);
  }

}