package com.mukund.PersonalProjectTool.exception.ProjectException;

public class ProjectException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public ProjectException(String message) {
		super(message);
	}
}
