package com.mukund.PersonalProjectTool.exception.BacklogException;

/**
 * ProjectTaskNotFoundException
 */
public class ProjectTaskNotFoundException extends RuntimeException {
  private static final long serialVersionUID = 1L;

  public ProjectTaskNotFoundException(String message) {
    super(message);
  }
}