package com.mukund.PersonalProjectTool.exception.BacklogException;

/**
 * BacklogException
 */
public class BacklogException extends RuntimeException {
  private static final long serialVersionUID = 1L;

  public BacklogException(String message) {
    super(message);
  }
}