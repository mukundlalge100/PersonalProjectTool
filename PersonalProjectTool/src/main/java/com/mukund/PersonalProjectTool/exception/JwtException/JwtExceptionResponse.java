package com.mukund.PersonalProjectTool.exception.JwtException;

import lombok.Data;

/**
 * JwtExceptionResponse
 */
@Data
public class JwtExceptionResponse {

  private Boolean success;
  private String message;
}