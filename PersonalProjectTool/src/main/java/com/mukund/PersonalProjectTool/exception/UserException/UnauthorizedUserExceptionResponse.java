package com.mukund.PersonalProjectTool.exception.UserException;

import lombok.Data;

/**
 * UnauthorizedUserExceptionResponse
 */
@Data
public class UnauthorizedUserExceptionResponse {

  private String message;

  public UnauthorizedUserExceptionResponse() {
    this.message = "Unauthorized,please logged in to get access";
  }

}