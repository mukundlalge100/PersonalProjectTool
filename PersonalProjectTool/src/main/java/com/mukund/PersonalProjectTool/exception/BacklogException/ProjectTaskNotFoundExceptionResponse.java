package com.mukund.PersonalProjectTool.exception.BacklogException;

import lombok.Data;

/**
 * ProjectTaskNotFoundExceptionResponse
 */
@Data
public class ProjectTaskNotFoundExceptionResponse {

  private String projectTaskNotFound;
}