package com.mukund.PersonalProjectTool.exception.ProjectException;

import lombok.Data;

/**
 * ProjectNotFoundExceptionResponse
 */
@Data
public class ProjectNotFoundExceptionResponse {

  private String projectNotFound;
  private Boolean success;
}