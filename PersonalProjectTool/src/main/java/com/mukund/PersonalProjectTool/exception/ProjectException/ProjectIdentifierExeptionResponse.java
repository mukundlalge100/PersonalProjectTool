package com.mukund.PersonalProjectTool.exception.ProjectException;

import lombok.Data;

@Data
public class ProjectIdentifierExeptionResponse {

	private String projectIdentifier;
}
