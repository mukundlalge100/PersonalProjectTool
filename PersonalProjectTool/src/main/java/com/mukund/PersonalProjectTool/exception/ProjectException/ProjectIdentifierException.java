package com.mukund.PersonalProjectTool.exception.ProjectException;

public class ProjectIdentifierException extends RuntimeException {
  private static final long serialVersionUID = 1L;

  public ProjectIdentifierException(String message) {
    super(message);
  }
}