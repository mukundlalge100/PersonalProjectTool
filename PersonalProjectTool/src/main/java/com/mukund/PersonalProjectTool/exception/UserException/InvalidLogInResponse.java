package com.mukund.PersonalProjectTool.exception.UserException;

import lombok.Data;

/**
 * InvalidLogInResponse
 */
@Data
public class InvalidLogInResponse {
  private String emailOrPasswordIsNotValid;

  public InvalidLogInResponse() {
    this.emailOrPasswordIsNotValid = "Email or password is not valid,please provide valid credentials!";
  }
}