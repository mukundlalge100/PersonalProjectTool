package com.mukund.PersonalProjectTool.exception.UserException;

/**
 * EmailAlreadyExistsException
 */
public class EmailAlreadyExistsException extends RuntimeException {
  private static final long serialVersionUID = 1L;

  public EmailAlreadyExistsException(String message) {
    super(message);
  }
}