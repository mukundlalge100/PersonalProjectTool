package com.mukund.PersonalProjectTool.mappers;

import com.mukund.PersonalProjectTool.entities.ProjectTask;
import com.mukund.PersonalProjectTool.request_response.ProjectTaskDto;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * ProjectTaskMapper
 */

@Mapper(componentModel = "spring")
public interface ProjectTaskMapper {
  ProjectTaskMapper INSTANCE = Mappers.getMapper(ProjectTaskMapper.class);

  ProjectTask projectTaskDtoToProjectTask(ProjectTaskDto projectTaskDto);

  ProjectTaskDto prokectTaskToProjectTaskDto(ProjectTask projectTask);

  Iterable<ProjectTaskDto> convert(Iterable<ProjectTask> projectTasks);
}