
package com.mukund.PersonalProjectTool.mappers;

import com.mukund.PersonalProjectTool.entities.Project;
import com.mukund.PersonalProjectTool.request_response.ProjectDto;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface ProjectMapper {
  ProjectMapper INSTANCE = Mappers.getMapper(ProjectMapper.class);

  Project projectDtoToProject(ProjectDto projectDto);

  ProjectDto projectToProjectDto(Project project);
}