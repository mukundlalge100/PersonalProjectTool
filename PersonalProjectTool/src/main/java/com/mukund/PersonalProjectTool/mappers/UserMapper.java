package com.mukund.PersonalProjectTool.mappers;

import com.mukund.PersonalProjectTool.response.SignUpResponse;
import com.mukund.PersonalProjectTool.entities.User;
import com.mukund.PersonalProjectTool.request.SignUpRequest;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * UserMapper
 */
@Mapper(componentModel = "spring")
public interface UserMapper {
  UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);

  User SignUpRequestToUser(SignUpRequest SignUpRequest);

  SignUpResponse userToSignUpResponse(User user);

}