package com.mukund.PersonalProjectTool.response;

import lombok.Data;

@Data
public class CheckProjectIdentifierExistResponse {
  private Boolean success;
  private String message;
}