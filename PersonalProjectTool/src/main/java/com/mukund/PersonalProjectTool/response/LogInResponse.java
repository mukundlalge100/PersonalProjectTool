package com.mukund.PersonalProjectTool.response;

import lombok.Data;

/**
 * LogInResponse
 */

@Data
public class LogInResponse {

  private Boolean success;
  private String token;

  public LogInResponse(Boolean success, String token) {
    this.success = success;
    this.token = token;
  }

  @Override
  public String toString() {
    return "LogInResponse [success=" + success + ", token=" + token + "]";
  }

}