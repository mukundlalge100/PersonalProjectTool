package com.mukund.PersonalProjectTool.response;

import lombok.Data;

/**
 * SignUpResponse
 */

@Data
public class SignUpResponse {

  private String email;
  private String userName;
  private String message;
  private boolean success;

  @Override
  public String toString() {
    return "User" + "email=" + email + ", userName=" + userName + "]";
  }
}