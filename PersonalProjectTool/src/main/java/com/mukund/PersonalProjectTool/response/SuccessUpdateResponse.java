package com.mukund.PersonalProjectTool.response;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class SuccessUpdateResponse {
  private String message;
  private boolean success;
}
