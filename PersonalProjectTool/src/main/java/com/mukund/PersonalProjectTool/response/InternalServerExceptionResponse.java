package com.mukund.PersonalProjectTool.response;

import java.time.LocalDateTime;

import lombok.Data;

@Data
public class InternalServerExceptionResponse {
  private String message;
  private String path;
  private Long status;
  private String error;
  private LocalDateTime timeStamp;

}
