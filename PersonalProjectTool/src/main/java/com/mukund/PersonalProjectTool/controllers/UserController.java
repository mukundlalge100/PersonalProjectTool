package com.mukund.PersonalProjectTool.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import com.mukund.PersonalProjectTool.constants.UrlMappings;
import com.mukund.PersonalProjectTool.Util.Util;
import com.mukund.PersonalProjectTool.request.LogInRequest;
import com.mukund.PersonalProjectTool.request.SignUpRequest;
import com.mukund.PersonalProjectTool.response.SignUpResponse;
import com.mukund.PersonalProjectTool.services.UserService;
import com.mukund.PersonalProjectTool.validators.UserValidator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * UserController api class which start api with /api/user
 */
@RestController
@CrossOrigin
@RequestMapping(UrlMappings.USER_URL)
public class UserController {

  private Logger LOGGER = LoggerFactory.getLogger(UserController.class);

  @Autowired
  private UserService userService;

  @Autowired
  private UserValidator userValidator;

  @Autowired
  private Util utilService;

  /**
   * Api for signup for user
   * 
   * @param signUpRequest - {@link SignUpRequest}
   * @param result        - {@link BindingResult}
   * @return ResponseEntity < ? >
   */
  @PostMapping(UrlMappings.SIGN_UP_URL)
  public ResponseEntity<?> signUp(@Valid @RequestBody SignUpRequest signUpRequest, BindingResult result) {
    LOGGER.info("UserController -> signUp -> STARTS");
    userValidator.validate(signUpRequest, result);
    ResponseEntity<?> errorsMap = utilService.getFieldErrors(result);
    if (errorsMap != null)
      return errorsMap;
    SignUpResponse signUpResponse = userService.createUser(signUpRequest);
    LOGGER.info("UserController -> signUp -> ENDS");
    return new ResponseEntity<SignUpResponse>(signUpResponse, HttpStatus.CREATED);
  }

  /**
   * Api for login functionality
   * 
   * @param logInRequest - {@link LogInRequest}
   * @param result       - {@link BindingResult}
   * @return ResponseEntity < ? >
   */
  @PostMapping(UrlMappings.LOGIN_URL)
  public ResponseEntity<?> logIn(@Valid @RequestBody LogInRequest logInRequest, BindingResult result) {
    LOGGER.info("UserController -> logIn -> STARTS");
    ResponseEntity<?> errorsMap = utilService.getFieldErrors(result);
    if (errorsMap != null)
      return errorsMap;
    ResponseEntity<?> responseEntity = userService.logIn(logInRequest);
    LOGGER.info("UserController -> logIn -> ENDS");
    return responseEntity;
  }

  /**
   * Api for logout functionality
   * 
   * @param request  - {@link HttpServletRequest}
   * @param response - {@link HttpServletResponse}
   * @return ResponseEntity < ? >
   */
  @GetMapping(UrlMappings.LOG_OUT_URL)
  public ResponseEntity<?> logOut(HttpServletRequest request, HttpServletResponse response) {
    LOGGER.info("UserController -> logOut -> STARTS");
    ResponseEntity<?> responseEntity = userService.logOut(request, response);
    LOGGER.info("UserController -> logOut -> ENDS");
    return responseEntity;
  }
}