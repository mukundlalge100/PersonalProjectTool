package com.mukund.PersonalProjectTool.controllers;

import javax.validation.Valid;

import com.mukund.PersonalProjectTool.constants.Messages;
import com.mukund.PersonalProjectTool.constants.UrlMappings;
import com.mukund.PersonalProjectTool.Util.Util;
import com.mukund.PersonalProjectTool.entities.ProjectTask;
import com.mukund.PersonalProjectTool.mappers.ProjectTaskMapper;
import com.mukund.PersonalProjectTool.request_response.ProjectTaskDto;
import com.mukund.PersonalProjectTool.response.SuccessUpdateResponse;
import com.mukund.PersonalProjectTool.services.BacklogService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 
 * @author Mukund lalge
 */
@RestController
@RequestMapping(UrlMappings.BACKLOG_URL)
@CrossOrigin
public class BacklogController {

  private Logger LOGGER = LoggerFactory.getLogger(BacklogController.class);

  @Autowired
  private BacklogService backlogService;

  @Autowired
  private Util utilService;

  /**
   * Api for create project task
   * 
   * @param projectTaskDto    - {@link ProjectTaskDto}
   * @param result            - {@link BindingResult}
   * @param projectIdentifier - {@link String}
   * @return ResponseEntity < ProjectTaskDto >
   */
  @PostMapping(UrlMappings.PROJECT_IDENTIFIER_URL)
  public ResponseEntity<?> createProjectTask(@Valid @RequestBody ProjectTaskDto projectTaskDto, BindingResult result,
      @PathVariable String projectIdentifier) {
    LOGGER.info("BacklogController -> createProjectTask -> STARTS");
    ResponseEntity<?> errorsMap = utilService.getFieldErrors(result);
    if (errorsMap != null)
      return errorsMap;
    ProjectTaskDto projectTaskDto2 = backlogService.createProjectTask(projectIdentifier, projectTaskDto);
    LOGGER.info("BacklogController -> createProjectTask -> ENDS");
    return new ResponseEntity<ProjectTaskDto>(projectTaskDto2, HttpStatus.CREATED);
  }

  /**
   * Api for getting all project tasks
   * 
   * @param projectIdentifier - {@link String}
   * @return Iterable < ProjectTaskDto >
   */
  @GetMapping(UrlMappings.PROJECT_IDENTIFIER_URL)
  public Iterable<ProjectTaskDto> getAllProjectTasks(@PathVariable String projectIdentifier) {
    LOGGER.info("BacklogController -> getAllProjectTasks -> STARTS");
    Iterable<ProjectTaskDto> projects = backlogService.getAllProjectTasks(projectIdentifier);
    LOGGER.info("BacklogController -> getAllProjectTasks -> ENDS");
    return projects;
  }

  /**
   * Api for getting project task
   * 
   * @param projectIdentifier - {@link String}
   * @param projectSequence   - {@link String}
   * @return ResponseEntity < ? >
   */
  @GetMapping(UrlMappings.PROJECT_TASK_URL)
  public ResponseEntity<?> getProjectTask(@PathVariable String projectIdentifier,
      @PathVariable String projectSequence) {
    LOGGER.info("BacklogController -> getProjectTask -> STARTS");
    ProjectTask projectTask = backlogService.getProjectTaskByProjectSequence(projectIdentifier, projectSequence);
    ProjectTaskDto projectTaskDto = ProjectTaskMapper.INSTANCE.prokectTaskToProjectTaskDto(projectTask);
    LOGGER.info("BacklogController -> getProjectTask -> ENDS");
    return new ResponseEntity<ProjectTaskDto>(projectTaskDto, HttpStatus.OK);
  }

  /**
   * Api for updating project
   * 
   * @param projectTaskDto    - {@link ProjectTaskDto}
   * @param result            - {@link BindingResult}
   * @param projectIdentifier - {@link String}
   * @param projectSequence   - {@link String}
   * @return ResponseEntity < ? >
   */
  @PatchMapping(UrlMappings.PROJECT_TASK_URL)
  public ResponseEntity<?> updateProjectTask(@Valid @RequestBody ProjectTaskDto projectTaskDto, BindingResult result,
      @PathVariable String projectIdentifier, @PathVariable String projectSequence) {
    LOGGER.info("BacklogController -> updateProjectTask -> STARTS");
    ResponseEntity<?> errorsMap = utilService.getFieldErrors(result);
    if (errorsMap != null)
      return errorsMap;
    ProjectTaskDto projectTaskDto2 = backlogService.updateProjectTask(projectTaskDto, projectIdentifier,
        projectSequence);
    LOGGER.info("BacklogController -> updateProjectTask -> ENDS");
    return ResponseEntity.ok(projectTaskDto2);
  }

  /**
   * Api for deleting project task
   * 
   * @param projectIdentifier - {@link String}
   * @param projectSequence   - {@link String}
   * @return ResponseEntity < ? >
   */
  @DeleteMapping(UrlMappings.PROJECT_TASK_URL)
  public ResponseEntity<SuccessUpdateResponse> deleteProjectTask(@PathVariable String projectIdentifier,
      @PathVariable String projectSequence) {
    LOGGER.info("BacklogController -> deleteProjectTask -> STARTS");
    backlogService.deleteProjectTask(projectIdentifier, projectSequence);
    SuccessUpdateResponse response = new SuccessUpdateResponse(
        String.format(Messages.PROJECT_TASK_DELETE_WITH_ID, projectSequence), Boolean.TRUE);

    LOGGER.info("BacklogController -> deleteProjectTask -> ENDS");
    return ResponseEntity.ok(response);
  }
}