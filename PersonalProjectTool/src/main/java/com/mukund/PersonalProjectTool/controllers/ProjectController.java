package com.mukund.PersonalProjectTool.controllers;

import java.text.ParseException;

import javax.validation.Valid;

import com.mukund.PersonalProjectTool.constants.Messages;
import com.mukund.PersonalProjectTool.constants.UrlMappings;
import com.mukund.PersonalProjectTool.Util.Util;
import com.mukund.PersonalProjectTool.request_response.ProjectDto;
import com.mukund.PersonalProjectTool.response.CheckProjectIdentifierExistResponse;
import com.mukund.PersonalProjectTool.response.SuccessUpdateResponse;
import com.mukund.PersonalProjectTool.services.ProjectService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = UrlMappings.PROJECT_URL)
@CrossOrigin
public class ProjectController {

	private Logger LOGGER = LoggerFactory.getLogger(ProjectController.class);

	@Autowired
	private ProjectService projectService;

	@Autowired
	private Util utilService;

	/**
	 * Api for creating project
	 * 
	 * @param projectDto - {@link ProjectDto}
	 * @param result     - {@link BindingResult}
	 * @return ResponseEntity < ? >
	 */
	@PostMapping
	public ResponseEntity<?> createProject(@Valid @RequestBody ProjectDto projectDto, BindingResult result) {
		LOGGER.info("ProjectController -> createProject -> STARTS");
		ResponseEntity<?> errorsMap = utilService.getFieldErrors(result);
		if (errorsMap != null)
			return errorsMap;
		ProjectDto projectDto2 = projectService.createProject(projectDto);
		LOGGER.info("ProjectController -> createProject -> ENDS");
		return new ResponseEntity<ProjectDto>(projectDto2, HttpStatus.CREATED);
	}

	/**
	 * Api for updating project
	 * 
	 * @param projectDto        - {@link ProjectDto}
	 * @param result            - {@link BindingResult}
	 * @param projectIdentifier - {@link String}
	 * @return ResponseEntity < ? >
	 */
	@PutMapping(UrlMappings.PROJECT_IDENTIFIER_URL)
	public ResponseEntity<?> updateProject(@Valid @RequestBody ProjectDto projectDto, BindingResult result,
			@PathVariable String projectIdentifier) {
		LOGGER.info("ProjectController -> updateProject -> STARTS");
		ResponseEntity<?> errorsMap = utilService.getFieldErrors(result);
		if (errorsMap != null)
			return errorsMap;
		ProjectDto projectDto2 = projectService.updateProject(projectDto, projectIdentifier);
		LOGGER.info("ProjectController -> updateProject -> ENDS");
		return new ResponseEntity<ProjectDto>(projectDto2, HttpStatus.ACCEPTED);
	}

	/**
	 * Api for get project by indentifier
	 * 
	 * @param projectIdentifier - {@link String}
	 * @return ResponseEntity < ? >
	 * @throws ParseException
	 */
	@GetMapping(UrlMappings.PROJECT_IDENTIFIER_URL)
	public ResponseEntity<?> getProjectByProjectIdentifier(@PathVariable String projectIdentifier) throws ParseException {
		LOGGER.info("ProjectController -> getProjectByProjectIdentifier -> STARTS");
		ProjectDto projectDto = projectService.findProjectByProjectIdentifier(projectIdentifier);
		LOGGER.info("ProjectController -> getProjectByProjectIdentifier -> ENDS");
		return new ResponseEntity<ProjectDto>(projectDto, HttpStatus.OK);
	}

	/**
	 * Api for find all projects with pagination
	 * 
	 * @param pageNumber - int
	 * @return Iterable < ProjectDto >
	 */
	@GetMapping(UrlMappings.GET_ALL_PROJECTS_URL)
	public Iterable<ProjectDto> findAllProjects(@PathVariable int pageNumber) {
		LOGGER.info("ProjectController -> findAllProjects -> STARTS");
		Pageable pageable = PageRequest.of(pageNumber, 4, Sort.by("startDate"));
		Iterable<ProjectDto> iterable = projectService.findAllProjects(pageable);
		LOGGER.info("ProjectController -> findAllProjects -> ENDS");
		return iterable;
	}

	/**
	 * Api for deleting project by identifier
	 * 
	 * @param projectIdentifier - {@link String}
	 * @return ResponseEntity < ? >
	 */
	@DeleteMapping(UrlMappings.PROJECT_IDENTIFIER_URL)
	public ResponseEntity<SuccessUpdateResponse> deleteProjectByProjectIdentifier(
			@PathVariable String projectIdentifier) {
		LOGGER.info("ProjectController -> deleteProjectByProjectIdentifier() ->STARTS");
		projectService.deleteProjectByProjectIdentifier(projectIdentifier);
		LOGGER.info("ProjectController -> deleteProjectByProjectIdentifier() ->ENDS");
		return ResponseEntity
				.ok(new SuccessUpdateResponse(String.format(Messages.PROJECT_DELETE_WITH_ID, projectIdentifier), true));
	}

	/**
	 * Api for checking if project is exists or not
	 * 
	 * @param projectDto - {@link ProjectDto}
	 * @return ResponseEntity < ? >
	 */
	@PostMapping(UrlMappings.CHECK_ID_EXIST_URL)
	@ResponseBody
	public ResponseEntity<?> checkProjectIdentifierExists(@RequestBody ProjectDto projectDto) {
		LOGGER.info("ProjectController -> checkProjectIdentifierExists -> STARTS");
		CheckProjectIdentifierExistResponse checkProjectIdentifierExistResponse = projectService
				.checkProjectIdentifierExists(projectDto.getProjectIdentifier());
		LOGGER.info("ProjectController -> checkProjectIdentifierExists -> ENDS");
		return new ResponseEntity<>(checkProjectIdentifierExistResponse, HttpStatus.OK);
	}

}