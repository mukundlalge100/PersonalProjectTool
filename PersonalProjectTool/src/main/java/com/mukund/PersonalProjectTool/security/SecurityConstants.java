package com.mukund.PersonalProjectTool.security;

/**
 * SecurityConstants
 */
public class SecurityConstants {
  public static final String[] AUTH_WHITELIST = { "/swagger-resources/**", "/swagger-ui.html", "/v2/api-docs",
      "/webjars/**" };

  public static final String USER_SIGNUP_URL = "/api/user/signup";
  public static final String USER_LOGIN_URL = "/api/user/login";
  public static final String USER_LOGOUT_URL = "/api/user/logout";
  public static final String SWAGER_URL = "/swagger-ui.html";
  public static final String SWAGER_DOCS = "/v2/api-docs";
  public static final String SECRET_KEY = "lms@123456";
  public static final String JWT_SECRET_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAik0ri47LT0ycxFocY00urcctrxl7dQFi6hyNZ5+0m4mSw/K+6xHMFKE0TriuNfaagNx617jQBpITJRBVy+d BcO04se0m6odhQq2eNWcX6TWxkDWDrILbMl72SswhKMeGevafm6MPbIsOCYC7fXJK6wbGWQzoBH3Cfb73gSopKJmJJxlrSeiEyUczy0dn1HusJboXw/QljzZdqcQl11LLSi0Qv5PAyymyYbKL7GliOMVE7wIay23CH+zqjh9zXsTwYyuW2HfqSKBi5qAJ2NwfmsA1EPUozJ/Z8honOQoKrPFH6PjOS/Vs9HPq+YKluSvWnlB+bMxEbsfzc5f8A5fzwIDAQAB";
  public static final String TOKEN_PREFIX = "Bearer ";
  public static final String HEADER_STRING = "Authorization";
  public static final long EXPIRATION_TIME = 864_000_00; // 24 hours ...
}