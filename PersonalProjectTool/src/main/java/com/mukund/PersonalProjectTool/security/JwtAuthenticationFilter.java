package com.mukund.PersonalProjectTool.security;

import java.io.IOException;
import java.util.Collections;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mukund.PersonalProjectTool.entities.User;
import com.mukund.PersonalProjectTool.services.CustomUserDetailsService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

/**
 * JwtAuthenticationFilter is class which filter http request checks if request
 * authorize or not!
 * 
 * @author mukund
 * @see JwtTokenProvider
 * 
 */
public class JwtAuthenticationFilter extends OncePerRequestFilter {

  @Autowired
  private JwtTokenProvider jwtTokenProvider;

  @Autowired
  private CustomUserDetailsService customUserDetailsService;

  @Override
  protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
      throws ServletException, IOException {
    String token = getJwtTokenFromRequest(request);
    if (StringUtils.hasText(token) && jwtTokenProvider.validateToken(token)) {
      Long userId = jwtTokenProvider.getUserIdFromJwtToken(token);
      User user = customUserDetailsService.findUserById(userId);
      UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(user, null,
          Collections.emptyList());
      authenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
      SecurityContextHolder.getContext().setAuthentication(authenticationToken);
    }
    filterChain.doFilter(request, response);
  }

  /**
   * Get jwt token from http request 
   * @param httpServletRequest - {@link HttpServletRequest}
   * @return String
   */ 
  private String getJwtTokenFromRequest(HttpServletRequest httpServletRequest) {
    String bearerToken = httpServletRequest.getHeader(SecurityConstants.HEADER_STRING);
    if (StringUtils.hasText(bearerToken) && bearerToken.startsWith(SecurityConstants.TOKEN_PREFIX)) {
      return bearerToken.substring(7, bearerToken.length());
    }
    return null;
  }
}