package com.mukund.PersonalProjectTool.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.mukund.PersonalProjectTool.exception.UserException.UnauthorizedUserExceptionResponse;

import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

/**
 * AccessDeniedHandler for handling unauthorized user exception
 *
 * @author Mukund Lalge
 */
@Component
public class AccessDeniedHandlerImpl implements AccessDeniedHandler {
  @Override
  public void handle(HttpServletRequest request, HttpServletResponse response,
      AccessDeniedException accessDeniedException) throws IOException, ServletException {
    UnauthorizedUserExceptionResponse exceptionResponse = new UnauthorizedUserExceptionResponse();
    String jsonString = new Gson().toJson(exceptionResponse);
    response.setContentType("application/json");
    response.setStatus(401);
    response.getWriter().print(jsonString);
  }

}