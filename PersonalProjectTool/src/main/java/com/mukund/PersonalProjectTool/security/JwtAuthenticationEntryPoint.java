package com.mukund.PersonalProjectTool.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.mukund.PersonalProjectTool.exception.UserException.InvalidLogInResponse;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

/**
 * JwtAuthenticationEntryPoint
 */
@Component
public class JwtAuthenticationEntryPoint implements AuthenticationEntryPoint {
  @Override
  public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException)
      throws IOException, ServletException {
    InvalidLogInResponse exceptionResponse = new InvalidLogInResponse();
    String jsonString = new Gson().toJson(exceptionResponse);
    response.setContentType("application/json");
    response.setStatus(401);
    response.getWriter().print(jsonString);
  }

}