package com.mukund.PersonalProjectTool.security;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.mukund.PersonalProjectTool.entities.User;

import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;

/**
 * JwtTokenProvider
 */
@Component
public class JwtTokenProvider {

  /**
   * JwtTokenProvider method for generating jwt token logic
   * 
   * @param authentication - {@link Authentication}
   * @return String
   */
  public String generateToken(Authentication authentication) {
    User user = (User) authentication.getPrincipal();
    Date now = new Date(System.currentTimeMillis());
    Date expiryDate = new Date(now.getTime() + SecurityConstants.EXPIRATION_TIME);
    Map<String, Object> claims = new HashMap<>();
    String userId = Long.toString(user.getUserId());
    claims.put("userId", user.getUserId());
    claims.put("email", user.getEmail());
    claims.put("userName", user.getUserName());
    return Jwts.builder().setSubject(userId).setClaims(claims).setIssuedAt(now).setExpiration(expiryDate)
        .signWith(SignatureAlgorithm.HS512, SecurityConstants.JWT_SECRET_KEY).compact();
  }

  /**
   * JwtTokenProvider method for validating token
   * 
   * @param token
   * @return
   */
  public boolean validateToken(String token) {
    try {
      Jwts.parser().setSigningKey(SecurityConstants.JWT_SECRET_KEY).parseClaimsJws(token);
      return true;
    } catch (SignatureException e) {
      throw new JwtException("Invalid JWT Signature");
    } catch (MalformedJwtException e) {
      throw new JwtException("Invalid JWT token");
    } catch (ExpiredJwtException e) {
      throw new JwtException("Expired JWT token");
    } catch (UnsupportedJwtException e) {
      throw new JwtException("Unsupported JWT Token");
    } catch (IllegalArgumentException e) {
      throw new JwtException("Token is empty or null");
    }
  }

  /**
   * JwtTokenProvider method for getting userId from jwt token
   * 
   * @param token
   * @return Long
   */
  public Long getUserIdFromJwtToken(String token) {
    Claims claims = Jwts.parser().setSigningKey(SecurityConstants.JWT_SECRET_KEY).parseClaimsJws(token).getBody();
    Integer userId = (Integer) claims.get("userId");
    return Long.parseLong(userId.toString());
  }
}