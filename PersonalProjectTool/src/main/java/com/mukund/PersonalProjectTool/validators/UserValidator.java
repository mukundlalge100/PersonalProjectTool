package com.mukund.PersonalProjectTool.validators;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.mukund.PersonalProjectTool.request.SignUpRequest;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 * SignUpValidations
 */

@Component
public class UserValidator implements Validator {

  @Override
  public boolean supports(Class<?> clazz) {
    return SignUpRequest.class.equals(clazz);
  }

  @Override
  public void validate(Object target, Errors errors) {
    SignUpRequest signUpRequest = (SignUpRequest) target;
    String expression = "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,64}$";

    Pattern pattern = Pattern.compile(expression);

    Matcher matcher = pattern.matcher(signUpRequest.getPassword());

    if (!matcher.matches()) {
      errors.rejectValue("password", "Bad Input",
          "Password must contain one or more uppercase letters,one or more lowercase letters,one or more special characters,one or more digit.");
    }
    if (!signUpRequest.getConfirmPassword().equals(signUpRequest.getPassword())) {
      errors.rejectValue("confirmPassword", "Match", "Passwords are not matching!");
    }
  }

}