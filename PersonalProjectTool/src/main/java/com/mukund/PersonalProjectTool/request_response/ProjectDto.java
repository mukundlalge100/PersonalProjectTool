package com.mukund.PersonalProjectTool.request_response;

import java.util.Date;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Data;

@Data
public class ProjectDto {
  @NotBlank(message = "Project name is required!!")
  private String projectName;

  @NotBlank(message = "Project identifier is required!")
  @Size(min = 4, max = 5, message = "Project Identifier should be between 4-5 characters only!")
  private String projectIdentifier;

  @NotBlank(message = "Project description is required!")
  private String description;

  @NotNull(message = "Start date is required!")
  private Date startDate;

  private String projectLeader;

  private Date endDate;

  @Override
  public String toString() {
    return "CreateProjectRequest [description=" + description + ", endDate=" + endDate + ", projectIdentifier="
        + projectIdentifier + ", projectLeader=" + projectLeader + ", projectName=" + projectName + ", startDate="
        + startDate + "]";
  }

}
