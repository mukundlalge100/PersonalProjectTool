package com.mukund.PersonalProjectTool.request_response;

import java.time.LocalDateTime;

import javax.validation.constraints.NotBlank;

import lombok.Data;

/**
 * Project task dto class
 */

@Data
public class ProjectTaskDto {
  private String acceptenceCriteria;
  private String status;
  private Integer priority;
  @NotBlank(message = "Summary is required!")
  private String summary;
  private String projectSequence;
  private LocalDateTime dueDate;
  private String projectIdentifier;

  @Override
  public String toString() {
    return "{acceptenceCriteria=" + getAcceptenceCriteria() + "'" + ", status='" + getStatus() + "'" + ", priority='"
        + getPriority() + "'" + ", summary='" + getSummary() + "'" + ", projectSequece='" + getProjectSequence() + "'"
        + ", dueDate='" + getDueDate();
  }
}