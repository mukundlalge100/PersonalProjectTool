package com.mukund.PersonalProjectTool.request_response;

import lombok.Data;

/**
 * BacklogDTO
 */
@Data
public class BacklogDto {
  private Integer PTSequence = 0;
  private String projectIdentifier;

  public BacklogDto() {
  }

  @Override
  public String toString() {
    return "{" + "'" + ", PTSequence='" + getPTSequence() + "'" + ", projectIdentifier='" + getProjectIdentifier() + "'"
        + "}";
  }

}