package com.mukund.PersonalProjectTool.constants;

public class UrlMappings {

  public static final String ROOT_URL = "/api";

  // BACKLOG ENDPOINTS
  public static final String BACKLOG_URL = ROOT_URL + "/backlog";
  public static final String PROJECT_IDENTIFIER_URL = "/{projectIdentifier}";
  public static final String PROJECT_TASK_URL = "/{projectIdentifier}/{projectSequence}";

  // PROJECT ENDPOINTS
  public static final String PROJECT_URL = ROOT_URL + "/project";
  public static final String GET_ALL_PROJECTS_URL = "/all/{pageNumber}";
  public static final String CHECK_ID_EXIST_URL = "/checkID";

  // USER AND AUTHENTICATION ENDPOINTS
  public static final String USER_URL = ROOT_URL + "/user";
  public static final String SIGN_UP_URL = "/signup";
  public static final String LOGIN_URL = "/login";
  public static final String LOG_OUT_URL = "/logout";
}
