package com.mukund.PersonalProjectTool.constants;

public class Messages {
  public static final String PROJECT_NOT_FOUND_ACCOUNT = "Project not found with your account!";
  public static final String PROJECT_NOT_FOUND_WITH_ID = "Project is not found with ID %s";
  public static final String PROJECT_TASK_NOT_FOUND = "Project task not found with ID %s";
  public static final String PROJECT_TASK_NOT_FOUND_WITH_PROJECT = "Project task not found with ID %s for project %s";

  public static final String PROJECT_DELETE_WITH_ID = "Project deleted  successfully with ID %s";

  public static final String PROJECT_TASK_DELETE_WITH_ID = "Project task deleted successfully with ID %s";
  public static final String SIGNUP_SUCCESSFULLY = "Sign up successfully.";
  public static final String SIGNUP_FAILED = "Sign up request failed.";
}
