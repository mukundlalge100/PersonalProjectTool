package com.mukund.PersonalProjectTool.Util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.mukund.PersonalProjectTool.entities.User;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

/**
 * Util
 */
@Component
public class Util {

  /**
   * Util format date method
   * 
   * @param dateString - {@link String}
   * @return Date
   */
  public Date formatDate(String dateString) {
    String formatString = dateString.replace("-", "/");
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
    Date myDate = null;
    try {
      myDate = dateFormat.parse(formatString);
    } catch (ParseException e) {
      e.printStackTrace();
    }
    return myDate;
  }

  /**
   * Util get fields errors method for controllers
   * 
   * @param result
   * @return ResponseEntity < ? >
   */
  public ResponseEntity<?> getFieldErrors(BindingResult result) {
    if (result.hasErrors()) {
      Map<String, String> errors = new HashMap<>();
      for (FieldError error : result.getFieldErrors()) {
        errors.put(error.getField(), error.getDefaultMessage());
      }
      return new ResponseEntity<Map<String, String>>(errors, HttpStatus.BAD_REQUEST);
    }
    return null;
  }

  /**
   * Util method for getting logged user name
   * 
   * @return String
   */
  public String getLoggedInUserName() {
    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    if (authentication != null) {
      User user = (User) authentication.getPrincipal();
      return user.getEmail();
    }
    return null;
  }
}