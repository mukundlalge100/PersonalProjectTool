package com.mukund.PersonalProjectTool.aspects;

import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.stereotype.Component;

@Component
@Aspect
@EnableAspectJAutoProxy(proxyTargetClass = true)
@ComponentScan("com.mukund.PersonalProjectTool")
public class ProjectAspect {

  private void log(Exception exception) {
    Logger logger = LoggerFactory.getLogger(this.getClass());
    logger.error("\nEXCEPTION CLASS: " + exception.getClass().getCanonicalName() + "\nEXCEPTION THROWN AT CLASS: "
        + exception.getStackTrace()[0].getClassName() + "\nAT METHOD: " + exception.getStackTrace()[0].getMethodName()
        + "\nAT LINE NUMBER: " + exception.getStackTrace()[0].getLineNumber() + "\nMESSAGE: " + exception.getMessage());

  }

  @AfterThrowing(pointcut = "execution(* com.mukund.PersonalProjectTool."
      + "services.ProjectService.*(..))", throwing = "exception")
  public void logExceptionFromProjectService(Exception exception) throws Exception {
    log(exception);
  }
}
