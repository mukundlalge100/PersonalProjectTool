package com.mukund.PersonalProjectTool.repositories;

import com.mukund.PersonalProjectTool.entities.Backlog;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * BackLogRepository
 */

@Repository
public interface BacklogRepository extends JpaRepository<Backlog, Integer> {
  public Backlog findByProjectIdentifier(String projectIdentifier);
}