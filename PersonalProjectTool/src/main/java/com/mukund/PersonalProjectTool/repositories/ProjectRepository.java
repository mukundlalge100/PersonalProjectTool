package com.mukund.PersonalProjectTool.repositories;

import com.mukund.PersonalProjectTool.entities.Project;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProjectRepository extends JpaRepository<Project, Integer> {
	public Project findByProjectIdentifier(String projectIdentifier);

	public Page<Project> findByProjectLeader(Pageable pageable, String projectLeader);

	public Page<Project> findAll(Pageable pageable);
}