package com.mukund.PersonalProjectTool.repositories;

import com.mukund.PersonalProjectTool.entities.User;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * UserRepository
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long> {

  public User findUserByEmail(String email);
}