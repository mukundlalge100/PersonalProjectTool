package com.mukund.PersonalProjectTool.repositories;

import com.mukund.PersonalProjectTool.entities.ProjectTask;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * ProjectTaskRepository
 */
@Repository
public interface ProjectTaskRepository extends JpaRepository<ProjectTask, Integer> {
  public Iterable<ProjectTask> findByProjectIdentifierOrderByPriorityDesc(String projectIdentifier);

  public ProjectTask findByProjectSequence(String projectSequence);
}
