package com.mukund.PersonalProjectTool.service;

import static org.mockito.Mockito.when;

import com.mukund.PersonalProjectTool.Util.TestUtil;
import com.mukund.PersonalProjectTool.Util.Util;
import com.mukund.PersonalProjectTool.exception.ProjectException.ProjectIdentifierException;
import com.mukund.PersonalProjectTool.mappers.ProjectMapper;
import com.mukund.PersonalProjectTool.repositories.BacklogRepository;
import com.mukund.PersonalProjectTool.repositories.ProjectRepository;
import com.mukund.PersonalProjectTool.repositories.UserRepository;
import com.mukund.PersonalProjectTool.request_response.ProjectDto;
import com.mukund.PersonalProjectTool.services.ProjectService;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mapstruct.factory.Mappers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest(classes = ProjectService.class)
public class ProjectServiceTest {

  @InjectMocks
  private ProjectService projectService;

  @Mock
  private ProjectRepository projectRepository;

  @Mock
  private BacklogRepository backlogRepository;

  @Mock
  private UserRepository userRepository;

  @Mock
  private Util utilService;

  @Spy
  private ProjectMapper projectMapper = Mappers.getMapper(ProjectMapper.class);

  @Before
  public void init() throws Exception {
    MockitoAnnotations.initMocks(this);
    when(userRepository.findUserByEmail(Mockito.anyString())).thenReturn(TestUtil.getUser());
    when(projectRepository.findByProjectIdentifier(Mockito.anyString())).thenReturn(TestUtil.getProject());
    when(utilService.getLoggedInUserName()).thenReturn(TestUtil.getUser().getEmail());
    when(projectRepository.save(Mockito.any())).thenReturn(TestUtil.getProject());
  }

  @Test(expected = ProjectIdentifierException.class)
  public void shouldThrowProjectIdentifierExceptionIfProjectAlreadyExist() throws Exception {
    projectService.createProject(TestUtil.getProjectDto());
  }

  @Test
  public void shouldSaveProjectIfAllProjectDataIsValid() throws Exception {
    when(projectRepository.findByProjectIdentifier(Mockito.anyString())).thenReturn(null);
    ProjectDto projectDto = projectService.createProject(TestUtil.getProjectDto());
    Assert.assertEquals(TestUtil.getProjectDto().getProjectIdentifier(), projectDto.getProjectIdentifier());
  }
}