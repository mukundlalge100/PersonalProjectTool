package com.mukund.PersonalProjectTool.service;

import com.mukund.PersonalProjectTool.services.BacklogService;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = BacklogService.class)
public class BacklogServiceTest {

}