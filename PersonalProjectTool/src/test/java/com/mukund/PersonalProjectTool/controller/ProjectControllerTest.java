package com.mukund.PersonalProjectTool.controller;

import static org.mockito.Mockito.when;

import com.mukund.PersonalProjectTool.Util.TestUtil;
import com.mukund.PersonalProjectTool.controllers.ProjectController;
import com.mukund.PersonalProjectTool.exception.ProjectException.ProjectNotFoundException;
import com.mukund.PersonalProjectTool.repositories.ProjectRepository;
import com.mukund.PersonalProjectTool.request_response.ProjectDto;
import com.mukund.PersonalProjectTool.services.ProjectService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;;

@RunWith(value = SpringRunner.class)
@SpringBootTest(classes = ProjectController.class)
public class ProjectControllerTest {

  @Autowired
  private ProjectController projectController;
  @MockBean
  private ProjectService projectService;
  @MockBean
  private ProjectRepository projectRepository;

  @Before
  public void init() throws Exception {
  }

  @Test
  public void statusCodeShouldBeOkForCheckProjectIndentifierExists() throws Exception {

  }

  @Test(expected = ProjectNotFoundException.class)
  public void statusCodeShouldBeNotFoundForCheckProjectIndentifierExists() throws Exception {
    ProjectDto projectDto = TestUtil.getProjectDto();
    projectDto = TestUtil.getProjectDto();
    projectDto.setProjectIdentifier("kjui");
    when(projectService.checkProjectIdentifierExists(projectDto.getProjectIdentifier()))
        .thenThrow(ProjectNotFoundException.class);
    when(projectRepository.findByProjectIdentifier(projectDto.getProjectIdentifier())).thenReturn(null);
    projectController.checkProjectIdentifierExists(projectDto);
  }
}