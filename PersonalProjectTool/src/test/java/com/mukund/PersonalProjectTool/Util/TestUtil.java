package com.mukund.PersonalProjectTool.Util;

import java.text.SimpleDateFormat;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mukund.PersonalProjectTool.Constants.Constants;
import com.mukund.PersonalProjectTool.entities.Project;
import com.mukund.PersonalProjectTool.entities.User;
import com.mukund.PersonalProjectTool.mappers.ProjectMapper;
import com.mukund.PersonalProjectTool.request_response.ProjectDto;

public class TestUtil {

  public static String jsonToString(Object object) throws Exception {
    ObjectMapper objectMapper = new ObjectMapper();
    return objectMapper.writeValueAsString(object);
  }

  public static ProjectDto getProjectDto() throws Exception {
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    ProjectDto projectDto = new ProjectDto();
    projectDto.setDescription("My demo project creted by mukundlalge");
    projectDto.setEndDate(null);
    projectDto.setProjectIdentifier("DBBGP");
    projectDto.setProjectName("Demo project-4");
    projectDto.setStartDate(dateFormat.parse("2019-10-07"));
    return projectDto;
  }

  public static Project getProject() throws Exception {
    Project project = ProjectMapper.INSTANCE.projectDtoToProject(getProjectDto());
    return project;
  }

  public static User getUser() {
    User user = new User();
    user.setEmail(Constants.EMAIL);
    user.setPassword(Constants.PASSWORD);
    user.setUserId(Constants.USERID);
    return user;
  }
}